package com.g2ssid.ddh_g2ssid_api.service;

import com.g2ssid.ddh_g2ssid_api.domain.model.department.Department;
import com.g2ssid.ddh_g2ssid_api.domain.model.project.Project;
import com.g2ssid.ddh_g2ssid_api.domain.repository.DepartmentRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.EmployeeRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.GenericRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.ProjectRepository;
import com.g2ssid.ddh_g2ssid_api.resources.project.AssignDepartmentsResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author Julio Quispe
 */
@Service
public class ProjectServiceImpl extends GenericServiceImpl<Project> implements ProjectService{

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DepartmentServicelmpl departmentServicelmpl;

    @Override
    protected GenericRepository<Project> getRepository() {
        return projectRepository;
    }

    public List<Department> assignDeparments(Long projectId, AssignDepartmentsResource assignDepartmentsResource) {
        List<Department> listDepartments = new ArrayList<>();
        Optional<Project> projectOp = projectRepository.findById(projectId);
        if (projectOp.isPresent()) {
            projectOp.get().setDepartments(new HashSet<>());
            for(Long departmentId : assignDepartmentsResource.getListDepartments()) {
                Optional<Department> departmentOp = departmentRepository.findById(departmentId);
                if(departmentOp.isPresent()) {
                    projectOp.get().getDepartments().add(departmentOp.get());
                    departmentOp.get().getProjects().add(projectOp.get());
                }
            }
            projectRepository.save(projectOp.get());
            listDepartments = departmentServicelmpl.getDepartmentsByProjectId(projectOp.get().getId());
        }
        return  listDepartments;
    }
}
