package com.g2ssid.ddh_g2ssid_api.service;

import com.g2ssid.ddh_g2ssid_api.domain.model.department.Department;
import com.g2ssid.ddh_g2ssid_api.domain.model.position.Position;
import com.g2ssid.ddh_g2ssid_api.domain.repository.DepartmentRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.GenericRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PositionServiceImpl extends GenericServiceImpl<Position> implements PositionService {

    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    protected GenericRepository<Position> getRepository() {
        return positionRepository;
    }

    public List<Position> getPositionsByDepartmentId(Long positionId) {
        Optional<Department> department = departmentRepository.findById(positionId);
        List<Position> positionsList = new ArrayList<>();
        if(department.isPresent()) {
            Set<Position> positionSet = department.get().getPositions();
            positionSet.forEach(positionsList::add);
        }
        return positionsList;
    }
}
