package com.g2ssid.ddh_g2ssid_api.service;

import com.g2ssid.ddh_g2ssid_api.domain.model.department.Department;
import com.g2ssid.ddh_g2ssid_api.domain.model.position.Position;
import com.g2ssid.ddh_g2ssid_api.domain.model.project.Project;
import com.g2ssid.ddh_g2ssid_api.domain.repository.DepartmentRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.PositionRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.GenericRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.ProjectRepository;
import com.g2ssid.ddh_g2ssid_api.resources.project.AssignPositionResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DepartmentServicelmpl extends GenericServiceImpl<Department> implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private PositionServiceImpl positionServiceImpl;

    @Override
    protected GenericRepository<Department> getRepository() {
        return departmentRepository;
    }

    public List<Department> getDepartmentsByProjectId(Long projectId) {
        Optional<Project> project = projectRepository.findById(projectId);
        List<Department> departmentsList = new ArrayList<>();
        if(project.isPresent()) {
            Set<Department> departmentSet = project.get().getDepartments();
            departmentSet.forEach(departmentsList::add);
        }
        return departmentsList;
    }

    public List<Position> assignDeparments(Long departmentId, AssignPositionResource assignPositionResource) {
        List<Position> listPositions = new ArrayList<>();
        Optional<Department> departmentOp = departmentRepository.findById(departmentId);
        if (departmentOp.isPresent()) {
            departmentOp.get().setPositions(new HashSet<>());
            for(Long positionId : assignPositionResource.getListPositions()) {
                Optional<Position> positionOp = positionRepository.findById(positionId);
                if(positionOp.isPresent()) {
                    departmentOp.get().getPositions().add(positionOp.get());
                    positionOp.get().getDepartments().add(departmentOp.get());
                }
            }
            departmentRepository.save(departmentOp.get());
            listPositions = positionServiceImpl.getPositionsByDepartmentId(departmentOp.get().getId());
        }
        return  listPositions;
    }

    @Override
    public List<Department> getDepartmentsByProject(Long projectId) {
        return departmentRepository.getDepartmentsByProject(projectId);
    }
}
