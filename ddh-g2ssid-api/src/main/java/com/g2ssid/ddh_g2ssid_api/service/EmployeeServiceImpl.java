package com.g2ssid.ddh_g2ssid_api.service;

import com.g2ssid.ddh_g2ssid_api.domain.model.employee.Employee;
import com.g2ssid.ddh_g2ssid_api.domain.model.equipment.Equipment;
import com.g2ssid.ddh_g2ssid_api.domain.model.equipment.EquipmentAssignment;
import com.g2ssid.ddh_g2ssid_api.domain.repository.EmployeeRepository;

import com.g2ssid.ddh_g2ssid_api.domain.repository.GenericRepository;

import com.g2ssid.ddh_g2ssid_api.domain.repository.*;
import com.g2ssid.ddh_g2ssid_api.resources.equipment.EquipmentControlDailyListResource;
import com.g2ssid.ddh_g2ssid_api.resources.equipment.EquipmentControlDailyResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * @author giovanny delgadillo
 */
@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void savePicture(Long id, InputStream inputStream) {
        Employee employeePersisted = findById(id);
        try {
            Byte[] bytes = ImageUtils.inputStreamToByteArray(inputStream);
            employeePersisted.setPicture(bytes);
            getRepository().save(employeePersisted);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public List<Equipment> getEquipmentAssignmentEmployee(Long employee_id){
        List<Equipment> equipmentResult = new ArrayList<>();
        Optional<Employee> employee = employeeRepository.findById(employee_id);
        if(employee.isPresent()){
            Set<EquipmentAssignment> AssignmentByEmployee = employee.get().getEquipmentAssignments();
            for (EquipmentAssignment equipmentAssignment: AssignmentByEmployee){
                if(equipmentAssignment.getActive()== true){}
            }
        }
        return equipmentResult;
    }
    @Override
    protected GenericRepository<Employee> getRepository() {
        return employeeRepository;
    }
}
