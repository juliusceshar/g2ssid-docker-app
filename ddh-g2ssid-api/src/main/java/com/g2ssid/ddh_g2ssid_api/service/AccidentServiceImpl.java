package com.g2ssid.ddh_g2ssid_api.service;

import com.g2ssid.ddh_g2ssid_api.domain.model.accident.Accident;
import com.g2ssid.ddh_g2ssid_api.domain.repository.AccidentRepository;
import com.g2ssid.ddh_g2ssid_api.domain.repository.GenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Michel Terrazas Mercado
 */

@Service
public class AccidentServiceImpl extends GenericServiceImpl <Accident> implements AccidentService{


    @Autowired
    private AccidentRepository accidentRepository;

    @Override
    protected GenericRepository<Accident> getRepository() {
        return accidentRepository;
    }
}



