-- employee items
insert into employee(id, first_name, last_name, gender, address, birth_date, ci, email, phone_number, picture, created_at, created_by, updated_at, updated_by) values(1, 'Tony', 'Stark', 'MALE', '10-4-80 Malibu Point', '1970-05-29', 12345678, 'ironman@startk.com', 123456789, LOAD_FILE('c:\\ProgramData\\MySQL\\MySQL Server 8.0\\UPLOADS\\1.jpg'), curdate(), 'importfile.sql', null, null);
insert into employee(id, first_name, last_name, gender, address, birth_date, ci, email, phone_number, picture, created_at, created_by, updated_at, updated_by) values(2, 'Thor', 'Odinson', 'MALE', 'Asgard', '1900-01-01', 4568123, 'thor@asgard.com', 285764531, LOAD_FILE('c:\\ProgramData\\MySQL\\MySQL Server 8.0\\UPLOADS\\2.jpg'), curdate(), 'import.sql', null, null);
insert into employee(id, first_name, last_name, gender, address, birth_date, ci, email, phone_number, picture, created_at, created_by, updated_at, updated_by) values(3, 'Natasha', 'Romanova', 'FEMALE', 'Rusia', '1980-03-20', 5784721, 'blackwidow@shield.com', 237461267, null, curdate(), 'import.sql', null, null);
insert into employee(id, first_name, last_name, gender, address, birth_date, ci, email, phone_number, picture, created_at, created_by, updated_at, updated_by) values(4, 'Wanda', 'Maximoff', 'FEMALE', 'Sokovia', '1985-02-14', 4673641, 'scarletwitch@avengers.com', 937556712, null, curdate(), 'import.sql', null, null);

-- contract items
insert into g2ssid.contract(id, employee_id, position_id, employee_type, salary, init_date, end_date, created_at, created_by, updated_at, updated_by) values(1, 1, 1, 'FULLTIME', 2000, '2017-03-20', null, curdate(), 'importfile.sql', null, null);
insert into g2ssid.contract(id, employee_id, position_id, employee_type, salary, init_date, end_date, created_at, created_by, updated_at, updated_by) values(2, 2, 2, 'HALFTIME', 2500, '2015-01-10', null, curdate(), 'importfile.sql', null, null);
insert into g2ssid.contract(id, employee_id, position_id, employee_type, salary, init_date, end_date, created_at, created_by, updated_at, updated_by) values(3, 3, 3, 'PARTTIME', 3140.67, '2018-03-20', '2018-12-20', curdate(), 'importfile.sql', null, null);
insert into g2ssid.contract(id, employee_id, position_id, employee_type, salary, init_date, end_date, created_at, created_by, updated_at, updated_by) values(4, 4, 4, 'PARTTIME', 2450.15, '2018-01-01', '2019-01-01', curdate(), 'importfile.sql', null, null);

-- equipment categories items
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(1, 'casco', 'Prenda protectora usada en la cabeza y hecha generalmente de metal o de algun otro material resistente, tipicamente para la proteccion de la cabeza contra objetos que caen', 'SAFETY', '/assets/images/helmet.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(2, 'guantes', 'Prenda protectora usada en las manos puede ser de varios tipos lates, goma, caucho, etc. su finalidad es proteger a las manos de golpes, rayones, calor extremo o sustancias dañinas', 'SAFETY', '/assets/images/gloves.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(3, 'audicion', 'disposito protector usado en los oidos, tipicamente usado para la proteccion del canal auditivo contra sonidos o ruidos extemadamente fuertes', 'SAFETY', '/assets/images/audio2.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(4, 'gafas', 'disposito protector usado en los ojos, tipicamente usado para la proteccion de los ojos contra golpes, luz extrema, y susstancias dañinas', 'SAFETY', '/assets/images/glasses.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(5, 'botas', 'disposito protector usado en los pies, tipicamente usado para la proteccion de los pies contra golpes, objetos que caen, calor extremo y sustancias dañinas', 'SAFETY', '/assets/images/boots.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(6, 'mask', 'disposito protector usado en la cara practicamente en el sector de la boca y la nariz, tipicamente usado para la proteccion de los pulmones contra gases toxicos', 'SAFETY', '/assets/images/mask.jpg', curdate(), 'importfile.sql', null, null);

insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(7, 'mezcladoras', 'Es una maquina para elaborar concreto y hormigón, facilitando y agilizando el trabajo del obrero en la mezcla', 'OPERATIONAL', '/assets/images/mezcla.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(8, 'estructuras', 'Son los equipos que generalmente se usan para llegar a partes altas en la construccion, estos pueden ser andamios, escaleras, elevadores, etc.', 'OPERATIONAL', '/assets/images/estructuras.jpg', curdate(), 'importfile.sql', null, null);
insert into equipment_category(id, name, description, equipment_type, image_path, created_at, created_by, updated_at, updated_by) values(9, 'herramientas', 'Son los distintos tipos de herramientas usados en la obra, como ser martillos, carretillas, etc.', 'OPERATIONAL', '/assets/images/tools.jpg', curdate(), 'importfile.sql', null, null);

-- equipment_category_characteristic
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(1, 'Hecha de polietileno de alta densidad y fibra sintética');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(1, 'Cuenta con una Visera que cubre el area de los ojos');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(1, 'Cuenta con un arnés para sujetar el casco de un material flexible');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(1, 'Cuenta con un Barboquejo que se acopla bajo la barbilla');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(1, 'Cuenta con ranura para linterna');

-- equipment_category_protect_to
insert into equipment_category_protect_to (equipment_category_id, protect_to) values(1, 'cabeza');
insert into equipment_category_protect_to (equipment_category_id, protect_to) values(1, 'ojos');
insert into equipment_category_protect_to (equipment_category_id, protect_to) values(1, 'orejas');
insert into equipment_category_protect_to (equipment_category_id, protect_to) values(1, 'rostro');

-- equipment_category_resistant_to
insert into equipment_category_resistant_to (equipment_category_id, resistant_to) values(1, 'Calor');
insert into equipment_category_resistant_to (equipment_category_id, resistant_to) values(1, 'Golpes Fuertes');
insert into equipment_category_resistant_to (equipment_category_id, resistant_to) values(1, 'Descargas Electricas');

-- equipment_category_characteristic
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(7, 'Mezcladora fijas al suelo');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(7, 'Mezcladoras moviles y portatiles con ruedas');
insert into equipment_category_characteristic (equipment_category_id, characteristic) values(7, 'Electricas y a gasolina');

-- equipment_category_resistant_to
insert into equipment_category_resistant_to (equipment_category_id, resistant_to) values(7, 'Altos voltajes');
insert into equipment_category_resistant_to (equipment_category_id, resistant_to) values(7, 'Golpes Fuertes');

-- equipments items
insert into equipment(id, code, state,available, acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(1, 'casco1', 'nuevo',true, '2017-03-20', 1, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available, acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(2, 'casco2', 'usado',true, '2018-03-20', 1, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(3, 'guantes amarillos', 'nuevo',true, '2017-11-10', 2, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(4, 'audifonos x', 'usado',true, '2018-04-20', 3, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(5, 'botas de caucho', 'nuevo',true, '2017-12-05', 5, curdate(), 'importfile.sql', null, null);

insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(6, 'botas de caucho', 'nuevo',true, '2017-12-05', 5, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(7, 'Lentes de proteccion', 'nuevo',true, '2017-12-05', 4, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(8, 'Casco', 'nuevo',true, '2017-12-05', 1, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(9, 'Guantes', 'nuevo',true, '2017-12-05', 2, curdate(), 'importfile.sql', null, null);

insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(10, 'botas de caucho', 'nuevo',true, '2017-12-05', 5, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(11, 'Lentes de proteccion', 'nuevo',true, '2017-12-05', 4, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(12, 'Casco', 'nuevo',true, '2017-12-05', 1, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(13, 'Guantes', 'nuevo',true, '2017-12-05', 2, curdate(), 'importfile.sql', null, null);

insert into equipment(id, code, state,available, acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(14, 'Mezcladora electrica', 'nuevo',true, '2018-01-20', 7, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available, acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(15, 'Mezcladora de gasolina', 'usado',true, '2017-03-20', 7, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(16, 'Andamio portatil', 'nuevo',true, '2017-11-10', 8, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(17, 'Elevador electrico', 'usado',true, '2018-02-11', 8, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(18, 'Carretilla', 'nuevo',true, '2016-09-10', 9, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(19, 'Pala', 'usado',true, '2018-03-05', 9, curdate(), 'importfile.sql', null, null);
insert into equipment(id, code, state,available ,acquisition_date, category_id, created_at, created_by, updated_at, updated_by) values(20, 'Martillo', 'nuevo',true, '2015-03-05', 9, curdate(), 'importfile.sql', null, null);

-- project items
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(1, 'project puente villa tunari ', 'des', 'place', 'CREATED', curdate(), 'importfile.sql', null, null);
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(2, 'project colegio sacaba', 'des', 'place', 'CREATED', curdate(), 'importfile.sql', null, null);
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(3, 'chapare1', 'des', 'chapare', 'ONHOLD', curdate(), 'importfile.sql', null, null);
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(4, 'shinaota', 'des', 'chapare', 'STARTED', curdate(), 'importfile.sql', null, null);
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(5, 'sacaba', 'des', 'sacaba', 'COMPLETED', curdate(), 'importfile.sql', null, null);
insert into g2ssid.project(id, name, description, place, status_project, created_at, created_by, updated_at, updated_by) values(6, 'cercado', 'des', 'cercado', 'COMPLETED', curdate(), 'importfile.sql', null, null);


-- departments items
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(1, 'Plantel de Obra Gruesa', 'Encargado de la obra gruesa. Cimientos, Estructura, Paredes', curdate(), 'importfile.sql', null, null);
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(2, 'Plantel de Obra Fina', 'Encargado de la obra fina de la construccion. Cubierta de techos, cielos rasos, revoques', curdate(), 'importfile.sql', null, null);
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(3, 'Plantel de encogrado', 'Encargado de la estructura metalica', curdate(), 'importfile.sql', null, null);
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(4, 'Plantel de Pintura', 'Encargado de pintura', curdate(), 'importfile.sql', null, null);
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(5, 'Instalaciones Electricas', 'Cableado Electrico', curdate(), 'importfile.sql', null, null);
insert into g2ssid.department(id, name, description, created_at, created_by, updated_at, updated_by) values(6, 'Instalaciones Sanitarias', 'Instalaciones desagues', curdate(), 'importfile.sql', null, null);



-- position items
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(1, 'Pintor ayudante', 'Pintado de las obras', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(2, 'Albañil ayudante', 'Realiza la construccion con supervicion en la obra gruesa y obra fina del proyecto asignado', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(3, 'Plomeria', 'Realiza el diseño, la instalacion y mantenimiento del alcantarillado y tuberias de agua', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(4, 'Tecnico Electricista', 'Realia la instalacion electrica de la obra.', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(5, 'Albañil maestro', 'Realiza la contruccion de la obra gruesa de la obra', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(6, 'Ing Electrico', 'Realiza el diseño, y los calculos de carga del sistemas electrico en la obra', curdate(), 'importfile.sql', null, null);
insert into g2ssid.position(id, name, description, created_at, created_by, updated_at, updated_by) values(7, 'Ing. Contruccion civil', 'Realiza el diseño, y calculo estructural', curdate(), 'importfile.sql', null, null);



-- project department
insert into g2ssid.project_department(project_id, department_id) values (1, 1);

-- department position
insert into g2ssid.department_position(department_id, position_id) values (1, 1);

-- security
INSERT INTO role (id, role_name, description, created_at, created_by, updated_at, updated_by) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin rights', curdate(), 'importfile.sql', null, null);
INSERT INTO role (id, role_name, description, created_at, created_by, updated_at, updated_by) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks', curdate(), 'importfile.sql', null, null);

-- USER
-- non-encrypted password: jwtpass
INSERT INTO user (id, first_name, last_name, email, password, username, created_at, created_by, updated_at, updated_by) VALUES (1, 'user1', 'user1', 'email@gmail.com', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'john.doe', curdate(), 'importfile.sql', null, null);
INSERT INTO user (id, first_name, last_name, email, password, username, created_at, created_by, updated_at, updated_by) VALUES (2, 'Admin', 'Admin', 'email@gmail.com', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'admin.admin', curdate(), 'importfile.sql', null, null);


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);

-- assign equipments
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 6, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 7, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (3, 1, 8, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 9, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);

INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (5, 2, 1, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (6, 2, 3, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);

INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (7, 3, 10, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (8, 3, 11, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (9, 3, 12, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);
INSERT INTO g2ssid.equipment_assignment (id, employee_id, equipment_id, active, init_date, end_date, created_at, created_by, updated_at, updated_by) VALUES (10, 3, 13, 1, curdate(), curdate() , curdate(), 'importfile.sql', null, null);