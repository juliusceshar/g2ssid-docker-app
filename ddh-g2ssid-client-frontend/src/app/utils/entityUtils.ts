export const EntityUtils = {
    buildFormUpdate : function(config, entity) {
        config.forEach(element => {
          const property = element.key;
          if (entity[property]) {
            element.value = entity[property];
          }
        });
    }
};
