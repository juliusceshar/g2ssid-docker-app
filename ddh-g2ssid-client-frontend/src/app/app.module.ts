import { NgxSpinnerModule } from 'ngx-spinner';
import {NgModule} from '@angular/core';
import {RestangularModule} from 'ngx-restangular';

// Function for setting the default restangular configuration
export function RestangularConfigFactory (RestangularProvider) {
  RestangularProvider.setBaseUrl('http://localhost:8080');
  // RestangularProvider.setDefaultHeaders({'Authorization': 'Bearer UDXPx-Xko0w4BRKajozCVy20X11MRZs1'});
}

// Modules
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { OrganizationalStructureModule } from './modules/structure-organizational/structure-organizational.module';
import { EquipmentInventoryModule } from './modules/equipment-inventory/equipment-inventory.module';
import { ToastrModule } from 'ngx-toastr';

// Components
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import { HomeComponent } from './home/home.component';

// Servicios
import {BaseService} from './services/base.service';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import {SharedModule} from './modules/shared/shared.module';
import { UserManagementModule } from './modules/user-management/user-management.module';
import { UserComponent } from './modules/user-management/user/user/user.component';
import { LoginComponent } from './modules/user-management/login/login.component';
import { ReportsModule } from './modules/reports/reports.module';
import { AccidentsModule } from './modules/accidents/accidents.module';
import { ControlDailyModule } from './modules/control-daily/control-daily.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    FormsModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule,
    SharedModule,
    OrganizationalStructureModule,
    EquipmentInventoryModule,
    UserManagementModule,
    ReportsModule,
    AccidentsModule,
    ControlDailyModule,
    RestangularModule.forRoot(RestangularConfigFactory)
  ],
  providers: [
  ],
  entryComponents: [
    LoginComponent
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
