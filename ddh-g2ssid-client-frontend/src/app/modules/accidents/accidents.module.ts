import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccidentFormComponent } from './accident-form/accident-form.component';
import { SharedModule } from '../shared/shared.module';
import {AccidentService} from './services/accident.service';
import { AccidentListComponent } from './accident-list/accident-list.component'
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule
  ],
  declarations: [AccidentFormComponent, AccidentListComponent, MainComponent],
  providers:[AccidentService],
  exports:[AccidentFormComponent]
})
export class AccidentsModule { }
