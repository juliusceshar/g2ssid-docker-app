import { Component, OnInit } from '@angular/core';
import {Accident} from './accident-form.config';
import { BaseComponent } from '../../../shared/baseComponent';
import { AccidentService } from '../services/accident.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from '../../structure-organizational/employee/services/employee.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-accident-form',
  templateUrl: './accident-form.component.html',
  styleUrls: ['./accident-form.component.scss']
})
export class AccidentFormComponent extends BaseComponent implements OnInit {
  metadata;
  employees: any;

  constructor(
    private accidentService: AccidentService,
    private employeeService: EmployeeService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(accidentService, Accident, router, route, toastr, spinner);
   }

   ngOnInit() {
    this.employeeService.getAll().subscribe(
      employees => {
        this.employees = employees;
        this.buildEmployeesDropdown();
        super.ngOnInit();
      }
    );
  }

  afterEditMode() {
    var valueSelected = this.metadata[0].value;
    this.metadata[0].value = _.find(this.metadata[0].options, option => {
      return option.key === valueSelected;
    });
  }

  buildEmployeesDropdown = function() {
    const options = [];
    this.employees.forEach(function(employee) {
      const name = employee.firstName + ' ' + employee.lastName;
      options.push({value: name, key: employee.id});
    });
    this.metadata[0].options = options;
  };
}
