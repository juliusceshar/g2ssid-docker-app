export const Accident = [
  {
    'key': 'employeeId',
    'label': 'Empleado',
    'type': 'dropdown',
    'required': true,
    'config': 'text',
    'value': ''
  },
    {
        'key': 'accidentType',
        'label': 'Tipo de accidente',
        'type': 'dropdown',
        'required': true,
        'value': {value: '', key: ''},
        'options': [
          {value: 'Personal', key: 'PERSONAL'},
          {value: 'Daños a la propiedad', key: 'DAMAGE'},
          {value: 'Medio ambiental', key: 'ENVIRONMENTAL'},
          {value: 'Fatalidad', key: 'FATALITY'},
        ]
      },
    {
        'key': 'cause',
        'label': 'Descripción accidente',
        'type': 'textarea',
        'required': true,
        'config': 'text'
      },
      {
        'key': 'dateInit',
        'label': 'Fecha de accidente',
        'type': 'datepicker',
        'Required': true
      },
      {
        'key': 'severity',
        'label': 'Evaluación de lesión',
        'type': 'dropdown',
        'required': true,
        'value': {value: '', key: ''},
        'options': [
          {value: 'Grave', key: 'SEVERE'},
          {value: 'Moderada', key: 'MODERATE'},
          {value: 'Leve', key: 'MILD'},
        ]
      },
      {        
        'key': 'receive',
        'label': 'Recibió',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Primeros auxilios', key: 'FIRSTAID'},
          {value: 'Tratamiento médico (fue llevado al hospital)', key: 'TREATMENT'},
         ]
      },
      {
        'key': 'assigned',
        'label': 'Reasignado a otra función',
        'type': 'RadioButton',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Si', key: 'YES'},
          {value: 'No', key: 'NOT'}
        ]
      },
      {
        'key': 'frecuency',
        'label': 'Reincidencia',
        'type': 'RadioButton',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Si', key: 'YES'},
          {value: 'No', key: 'NOT'}
        ]
      },
      {
        'key': 'turn',
        'label': 'Turno',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Mañana', key: 'MORNING'},
          {value: 'Tarde', key: 'AFTERNOON'},
          {value: 'Noche', key: 'NIGHT'},
        ]
      },

   {
        'key': 'way',
        'label': 'Forma de accidente',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Caída a nivel', key: 'LEVEL'},
          {value: 'Caída a altura', key: 'HIGHT'},
          {value: 'Caída al agua', key: 'WATER'},
          {value: 'Derrumbe o desplome de instalación', key: 'COLLAPSE'},
          {value: 'Caída de objetos', key: 'OBJECT'},
          {value: 'Pisada sobre objetos', key: 'STEP'},
          {value: 'Choque contra objetos', key: 'SHOCK'},
          {value: 'Golpe por objeto excepto caída', key: 'HIT'},
          {value: 'Aprisionamiento o atrapamiento', key: 'CATCH'},
          {value: 'Esfuerzo físico excesivo o falsos movimientos', key: 'EFFORT'},
          {value: 'Exposición al frio', key: 'COLD'},
          {value: 'Exposición al calor', key: 'HOT'},
        ]
      },
      {
        'key': 'kind',
        'label': 'Tipo de lesión',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Escoriaciones', key: 'SCORINGS'},
          {value: 'Herida punzante', key: 'STINGIN'},
          {value: 'Herida cortante', key: 'CUTTING'},
          {value: 'Herida contusa', key: 'BRUISE'},
          {value: 'Pérdida de tejido', key: 'TISSUELOSS'},
          {value: 'Contusión', key: 'CONTUSION'},
          {value: 'Luxación', key: 'LUXATION'},
          {value: 'Fractura', key: 'FRACTURE'},
          {value: 'Amputación', key: 'AMPUTATION'},
          {value: 'Quemadura', key: 'BURN'},
          {value: 'Cuerpo extraño en el ojo', key: 'EYE'},
          {value: 'Enucleación (Perdida ocular)', key: 'ENUCLEATION'},
        ]
      },
      {
        'key': 'body',
        'label': 'Parte del cuerpo lesionada',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Región craneana (cráneo, cuero cabelludo)', key: 'CRANIUM'},
          {value: 'Ojos', key: 'EYES'},
          {value: 'Boca', key: 'MOUTH'},
          {value: 'Cara', key: 'FACE'},
          {value: 'Nariz y senos paranasales', key: 'NOSE'},
          {value: 'Aparato auditivo', key: 'EAR'},
          {value: 'Cuello', key: 'NECK'},
          {value: 'Región cervical', key: 'CERVICAL'},
          {value: 'Región lumbosacra (columna vertebral y muscular adyacentes)', key: 'SPINE'},
          {value: 'Región dorsal', key: 'BACK'},
          {value: 'Tórax (costillas, esternón)', key: 'CHEST'},
          ]
      },
      {
        'key': 'factor',
        'label': 'Agente causante',
        'type': 'dropdown',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Piso', key: 'FLOOR'},
          {value: 'Paredes', key: 'WALL'},
          {value: 'Techo', key: 'ROOF'},
          {value: 'Escalera', key: 'STAIR'},
          {value: 'Rampas', key: 'RAMP'},
          {value: 'Pasarelas', key: 'GATEWAYS'},
          {value: 'Aberturas, puertas, portones', key: 'DOOR'},
          {value: 'Columnas', key: 'COLUMN'},
          {value: 'Ventanas', key: 'WINDOWS'},
          {value: 'Tubos de ventilacion', key: 'PIPES'},
          ]
      },
      {
        'key': 'medication',
        'label': 'Hubo tiempo perdido',
        'type': 'RadioButton',
        'value': {value: '', key: ''},
        'required': true,
        'options': [
          {value: 'Si', key: 'WITH'},
          {value: 'No', key: 'WITHOUT'}
        ]
      },
      {
        'key': 'startLostTime',
        'label': 'Inicio tiempo perdido',
        'type': 'datepicker',
        'Required': true
      },
      {
        'key': 'endLostTime',
        'label': 'Fin del tiempo perdido',
        'type': 'datepicker',
        'Required': true
      },
      {
        'key': 'dayLost',
        'label': 'Días perdidos',
        'type': 'textbox',
        'required': true,
        'config': 'text'
      },
      {
        'key': 'dayDebited',
        'label': 'Días debitados',
        'type': 'textbox',
        'required': true,
        'config': 'text'
      },
];
