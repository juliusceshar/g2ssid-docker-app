import { Injectable } from '@angular/core';
import { BaseService } from '../../../services/base.service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AccidentService extends BaseService {

  constructor(http: HttpClient) {
    super(http, 'accidents/');
  }
}
