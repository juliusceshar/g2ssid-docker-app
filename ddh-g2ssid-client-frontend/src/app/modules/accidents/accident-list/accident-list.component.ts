import { Component, OnInit } from '@angular/core';
import { AccidentService } from '../services/accident.service';
import { Accident } from '../accident-form/accident-form.config';
import { AccidentActionsConfig } from '../accident-form/accident.actions.config';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-accident-list',
  templateUrl: './accident-list.component.html',
  styleUrls: ['./accident-list.component.scss']
})
export class AccidentListComponent implements OnInit {

  metadata;
  accidents;
  actions;
  constructor(private accidentService: AccidentService,
    private router: Router,
    private route: ActivatedRoute) {
    this.metadata = Accident;
    this.actions = AccidentActionsConfig;
  }

  ngOnInit() {
    this.loadActions();
    this.loadAccidents();
  }

  loadAccidents() {
    this.accidentService.getAll().subscribe(result => this.accidents = result);
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
  }

  delete = (id) => {
    this.accidentService.delete(id).subscribe(res => this.loadAccidents());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
}
