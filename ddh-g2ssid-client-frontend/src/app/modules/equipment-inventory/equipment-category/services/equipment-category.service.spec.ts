import { TestBed, inject } from '@angular/core/testing';

import { EquipmentCategoryService } from './equipment-category.service';

describe('EquipmentCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EquipmentCategoryService]
    });
  });

  it('should be created', inject([EquipmentCategoryService], (service: EquipmentCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
