import { BaseService } from '../../../../services/base.service';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { EquipmentCategory } from '../../../../shared/equipment-category';
import {Restangular} from 'ngx-restangular';
import { baseURL } from '../../../../shared/baseurl';

@Injectable()
export class EquipmentCategoryService extends BaseService {
  private data: Observable<Array<object>>;
  getEquipmentsEndPoint: string;

  constructor(http: HttpClient,  private restangular: Restangular, private http2: HttpClient) {
  
      super(http, 'equipment-categories/');
      this.getEquipmentsEndPoint = baseURL + 'equipment-categories/';
  }

  getCategoriesEquipmentsByType(typeCategory:String): Observable<any> {
    return this.http2.get<any>(baseURL + "equipment-categories/categories/"+ typeCategory)
    .map((res) => {
      return res;
    }).catch(error => {
      console.log('error: ' + error);
      return error;
    });
  }

  getEquipmentsByCategory(categoryId:number): Observable<any> {
    return this.http2.get<any>(baseURL + "equipment-categories/"+ categoryId +"/equipments")
    .map((res) => {
      return res;
    }).catch(error => {
      console.log('error: ' + error);
      return error;
    });
  }


  getAllEquipments(id: string): Observable<any> {
    return this.http.get(this.getEquipmentsEndPoint + id + '/equipments')
    .map((res) => {
      return res;
    }).catch(error => {
      console.log('error: ' + error);
      return error;
    });
  }
}
