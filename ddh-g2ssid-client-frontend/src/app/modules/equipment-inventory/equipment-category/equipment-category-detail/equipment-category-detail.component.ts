import { Component, OnInit } from '@angular/core';
import { EquipmentCategoryService } from '../services/equipment-category.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipmentListconfig } from '../../equipment/equipment-list/equipment-list.config';
import { EquipmentActionsConfig } from '../../equipment/equipment/equipment.actions.config';
import { EquipmentService } from '../../equipment/services/equipment.service';

@Component({
  selector: 'app-equipment-category-detail',
  templateUrl: './equipment-category-detail.component.html',
  styleUrls: ['./equipment-category-detail.component.scss']
})
export class EquipmentCategoryDetailComponent implements OnInit {

  id;
  metadata;
  equipments: [any];
  actions;
  category: [any];

  pieChartLabels: string[] = ['Libres', 'Asignados'];
  pieChartData: number[] = [40, 60];
  pieChartType: string = 'pie';

  constructor(private equipmentCatService: EquipmentCategoryService,
    private equipmentService: EquipmentService,
    private router: Router,
    private route: ActivatedRoute) {

    this.metadata = EquipmentListconfig;
    this.actions = EquipmentActionsConfig;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    this.loadCategory();
    this.loadEquipments();
    this.loadActions();
  }

  loadCategory() {
    this.equipmentCatService.get(this.id).subscribe(result => this.category = result);
  }

  loadEquipments() {
    this.equipmentCatService.getAllEquipments(this.id).subscribe(result => this.equipments = result);
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
  }

  delete = (id) => {
    this.equipmentService.delete(id).subscribe(res => this.loadEquipments());
  }

  goToEdit = (id) => {
    this.router.navigate(['/equipment-inventory/equipments/' + id]);
  }

  // Chart events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }
}
