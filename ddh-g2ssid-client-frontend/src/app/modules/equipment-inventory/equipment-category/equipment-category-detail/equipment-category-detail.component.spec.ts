import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentCategoryDetailComponent } from './equipment-category-detail.component';

describe('EquipmentCategoryDetailComponent', () => {
  let component: EquipmentCategoryDetailComponent;
  let fixture: ComponentFixture<EquipmentCategoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentCategoryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentCategoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
