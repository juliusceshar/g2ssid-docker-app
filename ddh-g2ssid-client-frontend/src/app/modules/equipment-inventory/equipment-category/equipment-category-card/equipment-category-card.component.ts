import { EquipmentConstants } from './../../main/equipmentConstants';
import { EquipmentCategoryService } from '../services/equipment-category.service';
import { CardComponentConfig, CardItem } from '../../../shared/core/card/card-component.config';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-equipment-category-card',
  templateUrl: './equipment-category-card.component.html',
  styleUrls: ['./equipment-category-card.component.scss']
})
export class EquipmentCategoryCardComponent implements OnInit {

  categories: [any];
  type: any;
  cardCategory: CardComponentConfig;
  title: string;
  constructor(private equipmentCategoryService: EquipmentCategoryService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadEquipments();
  }

  loadEquipments() {
    let params = '?search=equipmentType:';
    if (this.route.snapshot.url[1].toString() === EquipmentConstants.OPERATIONAL) {
      params = params + EquipmentConstants.OPERATIONAL.toUpperCase();
      this.title = 'Equipo Operacional';
    } else {
      params = params + EquipmentConstants.SAFETY.toUpperCase();
      this.title = 'Equipos de Seguridad';
    }

    this.equipmentCategoryService.getAllWithSearch(params).subscribe(result => {
      this.categories = result;
      this.mapCategoriesToCard();
    });
  }

  mapCategoriesToCard() {
    const cardItems: CardItem[] = [];

    this.categories.forEach(element => {
      const item: CardItem = {
        id: element.id,
        name: element.name,
        image: element.imagePath,
        total: element.total
      };
      cardItems.push(item);
    });

    this.cardCategory = {
      cols: 4,
      height: '250px',
      gutterSize: '10px',
      title: this.title,
      createButton: {
        route: '../create',
        text: 'Agregar Nueva Categoria'
      },
      items: cardItems,
      delete: this.delete,
      edit: this.goToEdit,
      addItem: this.goToAddEquipment,
      details: this.goToDetails
    };
  }

  delete = (id) => {
    this.equipmentCategoryService.delete(id).subscribe(res => this.loadEquipments());
  }

  goToEdit = (id) => {
    this.router.navigate(['../' + id], {relativeTo: this.route});
  }

  goToAddEquipment = (id) => {
    this.router.navigate(['../../equipments/create/'], {relativeTo: this.route, queryParams: {catId: id}});
  }

  goToDetails = (id) => {
    this.router.navigate(['../detail/' + id], {relativeTo: this.route});
  }
}
