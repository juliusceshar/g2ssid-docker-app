import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentCategoryCardComponent } from './equipment-category-card.component';

describe('EquipmentCategoryCardComponent', () => {
  let component: EquipmentCategoryCardComponent;
  let fixture: ComponentFixture<EquipmentCategoryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentCategoryCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentCategoryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
