export const EquipmentCategoryConfig = [{
    'key': 'name',
    'label': 'Nombre',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'description',
    'label': 'Descripcion',
    'type': 'textarea',
    'required': true,
    'value': ''
},
{
    'key': 'equipmentType',
    'label': 'Tipo de Equipamiento',
    'type': 'RadioButton',
    'value': {value: '', key: ''},
    'required': true,
    'options': [
        {value: 'Equipo de Seguridad', key: 'SAFETY'},
        {value: 'Equipo Operacional', key: 'OPERATIONAL'}
    ]
},
{
    'key': 'imagePath',
    'type': 'file',
}
];
