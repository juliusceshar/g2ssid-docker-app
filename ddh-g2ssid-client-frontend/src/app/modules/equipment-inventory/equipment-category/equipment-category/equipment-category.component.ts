import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/baseComponent';
import { EquipmentCategoryService } from '../services/equipment-category.service';
import { EquipmentCategoryConfig } from './equipment-category.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-equipment-category',
  templateUrl: './equipment-category.component.html',
  styleUrls: ['./equipment-category.component.scss']
})
export class EquipmentCategoryComponent extends BaseComponent implements OnInit {

  urlToRedirect: String;
  constructor(
    private equipmentCategoryService: EquipmentCategoryService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(equipmentCategoryService, EquipmentCategoryConfig, router, route, toastr, spinner);
  }

  successCreated(category: any) {
    this.toastr.success('Fue creado exitosamente.', 'La Categoria: ' + category.name);
  }

  successEdited(category: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'La Categoria: ' + category.name);
  }

  successDeleted(category: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'La Categoria: ' + category.name);
  }

}
