import { PositionConfig } from '../../../structure-organizational/position/position/position.config';
import { EquipmentService } from '../services/equipment.service';
import { BaseComponent } from '../../../../shared/baseComponent';
import { Component, OnInit } from '@angular/core';
import { Equipmentconfig } from './equipment.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import {
    EquipmentCategoryService
} from '../../equipment-category/services/equipment-category.service';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.scss']
})
export class EquipmentComponent extends BaseComponent implements OnInit {

  categories: any;

  constructor(
    equipmentService: EquipmentService,
    private equipmentCategoryService: EquipmentCategoryService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(equipmentService, Equipmentconfig, router, route, toastr, spinner);
  }

  ngOnInit() {
    this.equipmentCategoryService.getAll().subscribe(
      categories => {
        this.categories = categories;
        this.buildEquipmentCategoryDropdown();
      }
    );
    super.ngOnInit();
  }

  buildEquipmentCategoryDropdown = function() {
    const options = [];
    this.categories.forEach(function(category) {
      options.push({value: category.name, key: category.id});
    });
    this.metadata[this.metadata.length - 1].options = options;
  };
}
