export const Equipmentconfig = [
{
    'key': 'code',
    'label': 'Codigo de Equipo',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'acquisitionDate',
    'label': 'Fecha de Adquisición',
    'type': 'datepicker',
    'required': true
},
{
    'key': 'state',
    'label': 'Estado',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'categoryId',
    'label': 'Categoria de Equipo',
    'type': 'dropdown',
    'required': true,
    'config': 'text',
    'value': '',
    'options': [
    ]
}
];
