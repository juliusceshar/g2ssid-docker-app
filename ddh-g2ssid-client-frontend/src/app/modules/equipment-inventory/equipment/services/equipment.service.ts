import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../../services/base.service';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EquipmentService extends BaseService {
    constructor(http: HttpClient) {
        super(http, 'equipment/');
    }
}
