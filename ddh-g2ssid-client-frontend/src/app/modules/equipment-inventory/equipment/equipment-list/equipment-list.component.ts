import { Component, OnInit } from '@angular/core';
import { EquipmentService } from '../services/equipment.service';
import { EquipmentListconfig } from './equipment-list.config';
import { EquipmentActionsConfig } from '../equipment/equipment.actions.config';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-equipment-list',
  templateUrl: './equipment-list.component.html',
  styleUrls: ['./equipment-list.component.scss']
})
export class EquipmentListComponent implements OnInit {

  metadata;
  equipments: [any];
  actions;

  constructor(private equipmentService: EquipmentService,
    private router: Router,
    private route: ActivatedRoute) {
    this.metadata = EquipmentListconfig;
    this.actions = EquipmentActionsConfig;
  }

  ngOnInit() {
    this.loadEquipments();
    this.loadActions();
  }

  loadEquipments() {
    this.equipmentService.getAll().subscribe(result => this.equipments = result);
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
  }

  delete = (id) => {
    this.equipmentService.delete(id).subscribe(res => this.loadEquipments());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], {relativeTo: this.route});
  }
}
