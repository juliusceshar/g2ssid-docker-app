export const EquipmentListconfig = [
    {
        'key': 'code',
        'label': 'Codigo de Equipo',
        'type': 'textbox'
    },
    {
        'key': 'acquisitionDate',
        'label': 'Fecha de Adquisición: dd/mm/aa',
        'type': 'datepicker'
    },
    {
        'key': 'state',
        'label': 'Estado',
        'type': 'textbox'
    },
    {
        'key': 'type',
        'label': 'Categoria de Equipo',
        'type': 'dropdown'
    }
];
