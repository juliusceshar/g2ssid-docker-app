import { Component, OnInit } from '@angular/core';
import { EquipmentAssignConfig } from './equipment-assign.config';
import { EquipmentAssignService } from './services/equipment-assign.service';
import { EmployeeService } from '../../structure-organizational/employee/services/employee.service';
import { EquipmentCategoryService } from '../equipment-category/services/equipment-category.service';
import { BaseComponent } from '../../../shared/baseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-equipment-assign',
  templateUrl: './equipment-assign.component.html',
  styleUrls: ['./equipment-assign.component.scss']
})
export class EquipmentAssignComponent extends BaseComponent implements OnInit {

  employees: any;
  categories: any;
  constructor(
    equipmentAssignService: EquipmentAssignService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService,
     private employeeService: EmployeeService,
     private categoryService: EquipmentCategoryService
  ) {
    super(equipmentAssignService, EquipmentAssignConfig, router, route, toastr, spinner);
  }

  ngOnInit() {
  this.employeeService.getAll().subscribe(employees => {this.employees = employees; this.buildEmployeeDropdown(); });
  this.categoryService.getAll().subscribe(categories => {this.categories = categories; this.buildCategoryDropdown(); });
 }

  successCreated(equipo: any) {
    this.toastr.success('El equipo: ' + equipo.name + 'fue asignado exitosamente.');
  }

  buildEmployeeDropdown = function() {
    const options = [];
    this.employees.forEach(function(employee) {
        options.push({ key: employee.id, value: employee.firstName + ' ' + employee.lastName});
    });
    this.metadata [0].options = options;
  };

  buildCategoryDropdown = function() {
    const options = [];
    this.categories.forEach(function(category) {
        options.push({ key: category.id, value: category.name});
    });
    this.metadata [2].options = options;
  };
}
