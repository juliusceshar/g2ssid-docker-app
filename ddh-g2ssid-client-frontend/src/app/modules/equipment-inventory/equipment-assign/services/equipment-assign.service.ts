import { BaseService } from '../../../../services/base.service';
import { Injectable } from '@angular/core';
import { EmployeeService } from '../../../structure-organizational/employee/services/employee.service';
import {EquipmentAssignConfig} from '../equipment-assign.config';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import { EquipmentCategoryService } from '../../equipment-category/services/equipment-category.service';


@Injectable()
export class EquipmentAssignService extends BaseService {

  employees: any;
  categories: any;
  metadata;

  constructor(http: HttpClient) {
    super(http, 'equipment/assign');

  }

}
