import { TestBed, inject } from '@angular/core/testing';

import { EquipmentAssignService } from './equipment-assign.service';

describe('EquipmentAssignService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EquipmentAssignService]
    });
  });

  it('should be created', inject([EquipmentAssignService], (service: EquipmentAssignService) => {
    expect(service).toBeTruthy();
  }));
});
