export const EquipmentAssignConfig = [
    {
        'key': 'equipmentType',
        'label': 'Tipo de Equipo',
        'type': 'dropdown',
        'required': true,
        'config': 'text',
        'value': '',
        'options': [
          {value: 'Equipo de Seguridad', key: 'SAFETY'},
          {value: 'Equipo Operacional', key: 'OPERATIONAL'}
        ]
      },
      {
        'key': 'equipmentCategory',
        'label': 'Categoria de Equipo',
        'type': 'dropdown',
        'required': true,
        'config': 'text',
        'value': '',
        'options': [
        ]
      },
    {
      'key': 'EquipmentId',
      'label': 'Equipo',
      'type': 'dropdown',
      'required': true,
      'config': 'number',
      'value': '',
      'options': [
      ]
    },
    ];

