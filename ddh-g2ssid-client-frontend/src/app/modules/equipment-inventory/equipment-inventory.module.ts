import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { EquipmentService } from './equipment/services/equipment.service';
import { CommonModule } from '@angular/common';
import { EquipmentComponent } from './equipment/equipment/equipment.component';
import { EquipmentInventoryComponent } from './main/equipment-inventory.component';
import { MatToolbarModule, MatButtonModule, MatGridListModule, MatListModule, MatCardModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { EquipmentAssignComponent } from './equipment-assign/equipment-assign.component';
import { EquipmentCategoryComponent } from './equipment-category/equipment-category/equipment-category.component';
import { EquipmentCategoryCardComponent } from './equipment-category/equipment-category-card/equipment-category-card.component';
import { EquipmentCategoryService } from './equipment-category/services/equipment-category.service';
import { EquipmentListComponent } from './equipment/equipment-list/equipment-list.component';
import { EquipmentAssignService } from './equipment-assign/services/equipment-assign.service';
import { EquipmentCategoryDetailComponent } from './equipment-category/equipment-category-detail/equipment-category-detail.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatListModule,
    RouterModule,
    MatCardModule,
    ChartsModule
  ],
  declarations: [
    EquipmentComponent,
    EquipmentInventoryComponent,
    EquipmentAssignComponent,
    EquipmentCategoryComponent,
    EquipmentCategoryCardComponent,
    EquipmentListComponent,
    EquipmentCategoryDetailComponent
  ],
  providers: [
    EquipmentService,
    EquipmentCategoryService,
    EquipmentAssignService,
  ],
  exports: [
    EquipmentComponent,
    RouterModule
  ]
})
export class EquipmentInventoryModule { }
