
export const ContractReportConfig = [
  {
    'key': 'employeeType',
    'label': 'Tipo de Empleado',
    'type': 'dropdown',
    'config': 'text',
    'value': '',
    'options': [
      {value: 'Tiempo completo', key: 'FULLTIME'},
      {value: 'Medio tiempo', key: 'HALFTIME'},
      {value: 'Tiempo parcial', key: 'PARTTIME'}
    ]
  },
 
  {
    'key': 'salary',
    'label': 'Salario',
    'type': 'textbox',
    'config': 'number',
    'value': ''
  },
  
];