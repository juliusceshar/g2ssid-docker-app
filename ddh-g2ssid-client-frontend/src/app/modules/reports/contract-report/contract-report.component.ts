import { Component, OnInit } from '@angular/core';
import { ContractService } from '../../structure-organizational/contract/services/contract.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContractReportConfig } from './contract-report.config';
import { BaseReport } from '../base-report';
import { BaseService } from '../../../services/base.service';
@Component({
  selector: 'app-contract-report',
  templateUrl: './contract-report.component.html',
  styleUrls: ['./contract-report.component.scss']
})

export class ContractReportComponent extends BaseReport {
  constructor(contractService: ContractService, spinner: NgxSpinnerService) { 
    super(contractService, spinner, ContractReportConfig);
 }
}
