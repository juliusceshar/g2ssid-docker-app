import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractReportComponent } from './contract-report.component';

describe('ContractReportComponent', () => {
  let component: ContractReportComponent;
  let fixture: ComponentFixture<ContractReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
