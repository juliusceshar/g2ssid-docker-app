import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../structure-organizational/project/services/project.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProjectReportConfig }  from './project-report.config';

@Component({
  selector: 'app-project-report',
  templateUrl: './project-report.component.html',
  styleUrls: ['./project-report.component.scss']
})
export class ProjectReportComponent {
  data: any;
  metadata: any;
  constructor(private projectService: ProjectService, private spinner: NgxSpinnerService) {
    this.metadata = ProjectReportConfig
  }

  save(value) {
    this.spinner.show();
    var params = JSON.stringify(value).replace(/['"]+/g, '');
    params = '?search=' + params.substring(1, params.length-1);
    this.projectService.getAllWithSearch(params).subscribe(
      res => {
        this.data = res;
        this.spinner.hide();
      });
  }
}
