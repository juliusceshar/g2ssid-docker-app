export const ProjectReportConfig = [{
  'key': 'name',
  'label': 'Nombre del Proyecto',
  'type': 'textbox',
  'required': false,
  'config': 'text',
  'value': ''
  },
  {
    'key': 'place',
    'label': 'Lugar del Proyecto',
    'type': 'textbox',
    'required': false,
    'config': 'text',
    'value': ''
  },
  {
    'key': 'statusProject',
    'label': 'Estado del Proyecto',
    'type': 'dropdown',
    'required': false,
    'config': 'text',
    'value': {key: '', value: ''},
    'options': [
      {value: 'Creado', key: 'CREATED', viewValue: 'Creado'},
      {value: 'Iniciado', key: 'STARTED', viewValue: 'Iniciado'},
      {value: 'Completado', key: 'COMPLETED', viewValue: 'Completado'},
      {value: 'Cancelado', key: 'CANCELLED', viewValue: 'Cancelado'},
      {value: 'Pausado', key: 'ONHOLD', viewValue: 'Pausado'}
    ]
  }
];
