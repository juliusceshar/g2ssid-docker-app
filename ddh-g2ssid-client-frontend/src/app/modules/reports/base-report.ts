import { Component, OnInit, Input } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { BaseService } from '../../services/base.service';

export class BaseReport {

  public data: any;
  constructor(protected baseService: BaseService, protected spinner: NgxSpinnerService, public metadata) {}

  save(value) {
    this.spinner.show();
    var params = JSON.stringify(value).replace(/['"]+/g, '');
    params = '?search=' + params.substring(1, params.length - 1);
    this.baseService.getAllWithSearch(params).subscribe(
    res => {
      this.data = res;
      this.spinner.hide();
    });
  }

}
