export const EquipmentCategoryReportConfig = [{
    'key': 'name',
    'label': 'Nombre',
    'type': 'textbox',
    'config': 'text',
    'value': ''
},
{
    'key': 'description',
    'label': 'Descripcion',
    'type': 'textarea',
    'value': ''
},
{
    'key': 'equipmentType',
    'label': 'Tipo de Equipamiento',
    'type': 'RadioButton',
    'value': {value: '', key: ''},
    'options': [
        {value: 'Equipo de Seguridad', key: 'SAFETY'},
        {value: 'Equipo Operacional', key: 'OPERATIONAL'}
    ]
},
];
