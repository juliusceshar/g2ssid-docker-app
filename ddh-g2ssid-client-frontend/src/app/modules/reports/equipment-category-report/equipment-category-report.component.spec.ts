import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentCategoryReportComponent } from './equipment-category-report.component';

describe('EquipmentCategoryReportComponent', () => {
  let component: EquipmentCategoryReportComponent;
  let fixture: ComponentFixture<EquipmentCategoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentCategoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentCategoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
