import { Component, OnInit } from '@angular/core';
import { EquipmentCategoryService } from '../../equipment-inventory/equipment-category/services/equipment-category.service';
import { BaseReport } from '../base-report';
import { NgxSpinnerService } from 'ngx-spinner';
import { EquipmentCategoryReportConfig } from './equipment-category-report.config';

@Component({
  selector: 'app-equipment-category-report',
  templateUrl: './equipment-category-report.component.html',
  styleUrls: ['./equipment-category-report.component.scss']
})
export class EquipmentCategoryReportComponent extends BaseReport {
    constructor(equipmentCategoryService: EquipmentCategoryService, spinner: NgxSpinnerService) {
    super(equipmentCategoryService, spinner, EquipmentCategoryReportConfig);
  }
}
