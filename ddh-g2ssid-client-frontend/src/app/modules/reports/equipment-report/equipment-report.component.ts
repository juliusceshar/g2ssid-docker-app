import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../../services/base.service';
import { BaseReport } from '../base-report';
import { EquipmentReportConfig } from './equipment-report.config';
import { EquipmentService } from '../../equipment-inventory/equipment/services/equipment.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-equipment-report',
  templateUrl: './equipment-report.component.html',
  styleUrls: ['./equipment-report.component.scss']
})
export class EquipmentReportComponent extends BaseReport {

  constructor(equipmentService: EquipmentService, spinner: NgxSpinnerService) {
    super(equipmentService, spinner, EquipmentReportConfig);
  }

  /**
   * Overrided to not affect the rest of reports, testing the option to have operators, in this case the [between] operator
   * if annother report needs to use this maybe we need to apply this in the BaseReport save method.
   * @param body the values retorned by the form
   */
  save(body) {
    this.spinner.show();
    let params = '?search=';
    Object.keys(body).forEach((key, index, array) => {
      let operator = ':'; // operator by default
      let value;
      if (body[key].operator !== undefined) {
        operator = body[key].operator;
        value = body[key].value;
      } else {
        value = body[key];
      }
      params = params + key + operator + value;
      if (index !== array.length - 1) {
        params = params + ',';
      }
    });
    params = encodeURI(params);
    this.baseService.getAllWithSearch(params).subscribe(
    res => {
      this.data = res;
      this.spinner.hide();
    });
  }
}
