export const EquipmentReportConfig = [
{
    'key': 'code',
    'label': 'Codigo',
    'type': 'textbox',
    'config': 'text',
    'value': ''
},
{
    'key': 'state',
    'label': 'Estado',
    'type': 'textbox',
    'config': 'text',
    'value': ''
},
{
    'key': 'acquisitionDate',
    'label': 'Fecha de Adquisición',
    'type': 'between',
    'config': 'text',
    'value1': '',
    'value2': '',
    'required': false,
    'operator': 'between'
}
];
