import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './main/report.component';
import { RouterModule } from '@angular/router';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule } from '@angular/material';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { EmployeeReportComponent } from './employee-report/employee-report.component';
import { ProjectReportComponent} from './project-report/project-report.component';
import { EquipmentReportComponent } from './equipment-report/equipment-report.component';
import { ContractReportComponent } from './contract-report/contract-report.component';
import { AccidentReportComponent } from './accident-report/accident-report.component';
import { DepartmentReportComponent } from './department-report/department-report.component';
import { EquipmentCategoryReportComponent } from './equipment-category-report/equipment-category-report.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule,
    PDFExportModule,
    MatFormFieldModule,
    MatInputModule
  ],

  declarations: [
    ReportComponent,
    EmployeeReportComponent,
    ProjectReportComponent,
    EquipmentReportComponent,
    ContractReportComponent,
    AccidentReportComponent,
    DepartmentReportComponent,
    EquipmentCategoryReportComponent
  ]
})
export class ReportsModule { }
