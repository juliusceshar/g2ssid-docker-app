export const EmployeeReportConfig = [{
    'key': 'firstName',
    'label': 'Nombre(s)',
    'type': 'textbox',    
    'config': 'text',
    'pattern': '[a-zA-Z ]*',
    'error': 'Solo letras son permitidas.',
    'value': ''
  },
{
    'key': 'lastName',
    'label': 'Apellidos',
    'type': 'textbox',    
    'config': 'text',
    'value': ''
},
{
    'key': 'ci',
    'label': 'C.I.',
    'type': 'textbox',    
    'config': 'number',
    'value': ''
},
{
    'key': 'phoneNumber',
    'label': 'Telefono',
    'type': 'textbox',    
    'config': 'number',
    'value': ''
},
{
    'key': 'gender',
    'label': 'Sexo:Masculino/Femenino',
    'type': 'dropdown',
    'value': {value: '', key: ''},
    'options': [
        {value: 'Femenino', key: 'FEMALE'},
        {value: 'Masculino', key: 'MALE'}
    ]
}
];
