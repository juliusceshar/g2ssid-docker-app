import { Component, OnInit } from '@angular/core';
import { EmployeeReportConfig } from './employee-report.config';
import { EmployeeService } from '../../structure-organizational/employee/services/employee.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { BaseService } from '../../../services/base.service';
import { BaseReport } from '../base-report';

@Component({
  selector: 'app-employee-report',
  templateUrl: './employee-report.component.html',
  styleUrls: ['./employee-report.component.scss']
})
export class EmployeeReportComponent extends BaseReport{
  constructor(employeeService: EmployeeService, spinner: NgxSpinnerService) {
    super(employeeService, spinner, EmployeeReportConfig);
  }
}
