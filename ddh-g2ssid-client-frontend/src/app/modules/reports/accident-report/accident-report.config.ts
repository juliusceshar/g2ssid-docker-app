export const AccidentReport = [
 
    {
        'key': 'accidentType',
        'label': 'Tipo de accidente',
        'type': 'dropdown',        
        'value': {value: '', key: ''},
        'options': [
          {value: 'Personal', key: 'PERSONAL'},
          {value: 'Daños a la propiedad', key: 'DAMAGE'},
          {value: 'Medio ambiental', key: 'ENVIRONMENTAL'},
          {value: 'Fatalidad', key: 'FATALITY'},
        ]
      },    
      {
        'key': 'severity',
        'label': 'Evaluación de lesión',
        'type': 'dropdown',        
        'value': {value: '', key: ''},
        'options': [
          {value: 'Grave', key: 'SEVERE'},
          {value: 'Moderada', key: 'MODERATE'},
          {value: 'Leve', key: 'MILD'},
        ]
      },
      {        
        'key': 'receive',
        'label': 'Recibió',
        'type': 'dropdown',
        'value': {value: '', key: ''},        
        'options': [
          {value: 'Primeros auxilios', key: 'FIRSTAID'},
          {value: 'Tratamiento médico (fue llevado al hospital)', key: 'TREATMENT'},
         ]
      },
      {
        'key': 'frecuency',
        'label': 'Reincidencia',
        'type': 'RadioButton',
        'value': {value: '', key: ''},        
        'options': [
          {value: 'Si', key: 'YES'},
          {value: 'No', key: 'NOT'}
        ]
      },
      {
        'key': 'turn',
        'label': 'Turno',
        'type': 'dropdown',
        'value': {value: '', key: ''},        
        'options': [
          {value: 'Mañana', key: 'MORNING'},
          {value: 'Tarde', key: 'AFTERNOON'},
          {value: 'Noche', key: 'NIGHT'},
        ]
      },
   {
        'key': 'way',
        'label': 'Forma de accidente',
        'type': 'dropdown',
        'value': {value: '', key: ''},        
        'options': [
          {value: 'Caída a nivel', key: 'LEVEL'},
          {value: 'Caída a altura', key: 'HIGHT'},
          {value: 'Caída al agua', key: 'WATER'},
          {value: 'Derrumbe o desplome de instalación', key: 'COLLAPSE'},
          {value: 'Caída de objetos', key: 'OBJECT'},
          {value: 'Pisada sobre objetos', key: 'STEP'},
          {value: 'Choque contra objetos', key: 'SHOCK'},
          {value: 'Golpe por objeto excepto caída', key: 'HIT'},
          {value: 'Aprisionamiento o atrapamiento', key: 'CATCH'},
          {value: 'Esfuerzo físico excesivo o falsos movimientos', key: 'EFFORT'},
          {value: 'Exposición al frio', key: 'COLD'},
          {value: 'Exposición al calor', key: 'HOT'},
        ]
      },
      {
        'key': 'factor',
        'label': 'Agente causante',
        'type': 'dropdown',
        'value': {value: '', key: ''},        
        'options': [
          {value: 'Piso', key: 'FLOOR'},
          {value: 'Paredes', key: 'WALL'},
          {value: 'Techo', key: 'ROOF'},
          {value: 'Escalera', key: 'STAIR'},
          {value: 'Rampas', key: 'RAMP'},
          {value: 'Pasarelas', key: 'GATEWAYS'},
          {value: 'Aberturas, puertas, portones', key: 'DOOR'},
          {value: 'Columnas', key: 'COLUMN'},
          {value: 'Ventanas', key: 'WINDOWS'},
          {value: 'Tubos de ventilacion', key: 'PIPES'},
          ]
      }
];
