import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccidentReportComponent } from './accident-report.component';

describe('AccidentReportComponent', () => {
  let component: AccidentReportComponent;
  let fixture: ComponentFixture<AccidentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccidentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccidentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
