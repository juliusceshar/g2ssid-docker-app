import { BaseReport } from './../base-report';
import { AccidentReport } from './accident-report.config';
import { Component, OnInit } from '@angular/core';
import { AccidentService } from '../../accidents/services/accident.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-accident-report',
  templateUrl: './accident-report.component.html',
  styleUrls: ['./accident-report.component.scss']
})
export class AccidentReportComponent extends BaseReport {

  constructor(accidentService: AccidentService, spinner: NgxSpinnerService) {
    super(accidentService, spinner, AccidentReport);
   }
}
