export const DepartmentReportConfig = [{
    'key': 'name',
    'label': 'Nombre del Departamento',
    'type': 'textbox',
    'config': 'text',
    'pattern': '[a-zA-Z ]*',
    'error': 'Solo letras son permitidas.',
    'value': ''
  },
    {
      'key': 'description',
      'label': 'Descripcion del Departamento',
      'type': 'textarea',
      'config': 'textarea',
      'value': ''
    }
  ];