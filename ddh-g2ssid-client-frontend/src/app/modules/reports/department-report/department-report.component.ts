import { BaseReport } from './../base-report';
import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../structure-organizational/department/services/department.service';
import { NgxSpinnerComponent, NgxSpinnerService } from 'ngx-spinner';
import { DepartmentReportConfig } from './department-report.config';

@Component({
  selector: 'app-department-report',
  templateUrl: './department-report.component.html',
  styleUrls: ['./department-report.component.scss']
})
export class DepartmentReportComponent extends BaseReport {

  constructor(departmentService: DepartmentService, spinner: NgxSpinnerService) { 
    super(departmentService, spinner, DepartmentReportConfig);
  }

}
