import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {UserComponent} from './user/user/user.component';
import {LoginComponent} from './login/login.component';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatDialogModule, MatCheckboxModule } from '@angular/material';
import {FormsModule} from '@angular/forms';
import { UserManagementComponent } from './main/user-management.component';
import { UserListComponent } from './user/user-list/user-list.component';
import {MatSelectModule} from '@angular/material/select';
import { UserService } from './user/services/user.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatCheckboxModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
  ],
  declarations: [
    UserComponent,
    LoginComponent,
    UserManagementComponent,
    UserListComponent],
  exports: [
    UserComponent,
    UserListComponent,
    LoginComponent
  ],
  providers: [ 
    UserService,
  ],
  entryComponents: [
    UserComponent
  ]
})
export class UserManagementModule { }
