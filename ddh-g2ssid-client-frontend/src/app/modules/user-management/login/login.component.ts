import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user;
// user = {remeber: false};

  constructor(public dialogRef: MatDialogRef<LoginComponent>) {
    this.user = {
      name: '',
      password: ''
    };
   }

  ngOnInit() {
  }

  onSubmit() {
    this.dialogRef.close();
  }

}
