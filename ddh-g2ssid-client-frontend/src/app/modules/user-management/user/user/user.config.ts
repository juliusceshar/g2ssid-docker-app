export const UserConfig = [{
    'key': 'firstName',
    'label': 'Nombre(s)',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'pattern': '[a-zA-Z ]*',
    'error': 'Solo letras son permitidas.',
    'value': ''
  },
{
    'key': 'lastName',
    'label': 'Apellidos',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'userName',
    'label': 'Usuario',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'password',
    'label': 'Contraseña',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},

{
    'key': 'email',
    'label': 'Email',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
}

];
