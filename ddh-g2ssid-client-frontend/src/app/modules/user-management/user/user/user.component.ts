import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import { BaseComponent } from '../../../../shared/baseComponent';
import {UserService} from '../services/user.service';
import {UserConfig} from './user.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends BaseComponent implements OnInit {

  constructor(
   private userService: UserService,
   router: ActivatedRoute,
   route: Router,
   spinner: NgxSpinnerService,
   toastr: ToastrService){
    super(userService, UserConfig, router, route, toastr, spinner);
  }

  successCreated(user: any) {
    this.toastr.success('Fue creado exitosamente.', 'Usuario: ' + user.name);
  }

  successEdited(user: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'Usuario: ' + user.name);
  }

  successDeleted(user: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'Usuario: ' + user.name);
  }

}


