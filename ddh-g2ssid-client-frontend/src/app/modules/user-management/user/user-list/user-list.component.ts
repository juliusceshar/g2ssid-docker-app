
import { Component, OnInit } from '@angular/core';
import {UserConfig} from '../user/user.config';
import {UserService} from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersActionsConfig } from '../user/user.actions.config';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  metadata;
  users: [any];
  actions;
  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) { 
      this.metadata = UserConfig;
      this.actions = UsersActionsConfig;
  }

  ngOnInit() {
    this.loadUsers();
    this.loadActions();
  }

  loadUsers() {
    this.userService.getAll().subscribe(result => this.users = result );
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
  }

  delete = (id) => {
    this.userService.delete(id).subscribe(res => this.loadUsers());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
}
