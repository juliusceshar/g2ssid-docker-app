import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { UserComponent } from '../user/user/user.component';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
