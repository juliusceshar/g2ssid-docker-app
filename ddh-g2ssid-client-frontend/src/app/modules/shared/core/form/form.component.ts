import { Component, Input, Output, EventEmitter, OnInit, AfterContentChecked, AfterViewInit } from '@angular/core';
import {  NgSwitch, NgSwitchCase } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input()
  items: [any];

  @Input()
  labelButton;

  @Output()
  save: EventEmitter<any> = new EventEmitter<any>();
  url: String;
  disabled: boolean;

  constructor(public toastr: ToastrService) {
    this.labelButton = 'Guardar';
  }

  ngOnInit() {
    this.disabled = false;
    this.readValues();
  }

  emitSaveEvent() {
    const body = this.getValues();
    if(!this.disabled) {
      this.save.emit(body);
    }
  }

  readValues() {
    this.items.forEach(function(field) {
      if (field.type === 'datepicker') {
        var date = new Date(field.value);
        field.value = date;
      }
    });
  }

  getValues() {
    const body = {};
    var self = this;
    this.items.forEach(function(field) {
      if (typeof field.value == 'string') {
        field.value = _.trim(field.value);
      }
      if (field.type === 'datepicker' && field.value !== undefined && (typeof field.value) !== 'string') {
        field.value = field.value == null ? '' : field.value.toISOString();
      } else if (field.type === 'dropdown' || field.type === 'RadioButton') {
        if (field.value !== undefined) {
          field.value = field.value.key;
        }
      }
      if (field.type === 'between' && field.value1 !== undefined && field.value2 !== undefined && (typeof field.value1) !== 'string') {
          field.value1 = field.value1 == null ? '' : field.value1.getFullYear() + '-' + (field.value1.getMonth()+1) + '-' + field.value1.getDate();
          field.value2 = field.value2 == null ? '' : field.value2.getFullYear() + '-' + (field.value2.getMonth()+1) + '-' + field.value2.getDate();
          field.value = field.value1 + ';' + field.value2;
      }
      if (!_.isUndefined(field.value) &&
        ((typeof field.value == 'string' && field.value !== '') || (typeof field.value == 'number'))) {
        if (field.operator !== undefined) {
          body[field.key] = {
            'value': field.value,
            'operator': '[' + field.operator + ']'
          };
        } else {
          body[field.key] = field.value;
        }
        self.disabled = false;
      } else {
        if (field.required) {
          self.disabled = true;
          self.toastr.error('Se encontraron errores en el formulario, por favor complete la informacion requerida.');
          return;
        }
      }
    });
    console.log(body);
    return body;
  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (eventSelectedImage: any) => {
        this.url = eventSelectedImage.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  hasError(item) {
    let hasError = false;
    if (item) {
      const regex = new RegExp(item.pattern);
      hasError = regex.test(item.value);
    }
    return hasError;
  }

  getDate(item) {
    item.value = new Date(item.value);
  }
}
