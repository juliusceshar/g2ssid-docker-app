import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import 'rxjs/add/observable/from';
import * as _ from 'lodash';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input()
  item;

  @Input()
  selected = {key: '', value: ''};

  @Output()
  selectedChange: EventEmitter<any> = new EventEmitter<any>();

  filter: [any];
  value: String;

  constructor() {
  }

  ngOnInit() {
    this.value = this.selected.value;
    this.filter = this.item.options;
  }

  modelChange(newSelected) {
    if (typeof newSelected === 'string') {
      this.value = newSelected;
      if (newSelected === '') {
        this.filter = this.item.options;
        this.selected = undefined;
        return;
      }
      this.filter = this.item.options.filter(option =>
        option.value.toLowerCase().indexOf(newSelected.toLowerCase()) > -1);
      this.selected = {key: '', value: newSelected};
      this.selected = _.find(this.item.options, option => {
        return option.value.toLowerCase() === this.value.toLowerCase();
      });
      if (this.selected) {
        this.value = this.selected.value;
      }
    }
  }

  validate() {
    if (!this.selected) {
      this.value = '';
      this.filter = this.item.options;
    }
    this.selectedChange.emit(this.selected);
  }
}
