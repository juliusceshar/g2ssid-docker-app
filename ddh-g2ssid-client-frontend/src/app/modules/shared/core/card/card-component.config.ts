export interface CardComponentConfig {
    title: string;
    cols: number;
    height: string;
    gutterSize: string;
    createButton: {
        text: string;
        route: string;
    };
    items: CardItem[];

    edit(id: string);
    delete(id: string);
    addItem(id: string);
    details(id: string);
}

export interface CardItem {
    id: number;
    name: string;
    image: string;
    total: number;
}
