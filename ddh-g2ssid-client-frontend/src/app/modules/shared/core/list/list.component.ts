import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import {  NgSwitch, NgSwitchCase } from '@angular/common';
import {DataSource} from '@angular/cdk/table';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input()
  metadata;
  @Input()
  metaDataActions;
  @Input()
  data;
  headers;
  sortObject;

  constructor() {}
  ngOnInit() {
    this.headers = this.getheaders();
    this.sortObject = {header: '', orderAsc: true};
  }

  getheaders() {
    const columns = [];
    this.metadata.forEach(function(field) {
      columns.push({label: field.label, key: field.key, type: field.type});
    });
    if (this.metaDataActions !== undefined) {
      columns.push({label: 'Acciones', key: 'Actions'});
    }
    return columns;
  }

  getDate(value) {
    return (new Date(value)).toLocaleDateString();
  }

  sort(column) {
    if (this.sortObject.header === column) {
      this.sortObject.orderAsc = !this.sortObject.orderAsc;
    } else {
      this.sortObject.header = column;
      this.sortObject.orderAsc = true;
    }
    const order = this.sortObject.orderAsc ? 'asc' : 'desc';
    this.data = _.orderBy(this.data, [item => item[column].toString().toLowerCase()], [order]);
  }
}
