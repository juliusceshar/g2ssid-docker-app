import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentAssignComponent } from './equipment-assign.component';

describe('EquipmentAssignComponent', () => {
  let component: EquipmentAssignComponent;
  let fixture: ComponentFixture<EquipmentAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
