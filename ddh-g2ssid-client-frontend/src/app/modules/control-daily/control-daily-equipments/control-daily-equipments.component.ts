import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { EquipmentAssignedListConfig} from './equipment-assigned-list.config';
import { ControlDailyEquipmentsActionsConfig} from './control-daily-equipments.actions.config';
import * as _ from "lodash";
import { ControlDailyEquipmentsService } from '../services/control-daily-equipments.service';
import {EquipmentService} from '../../equipment-inventory/equipment/services/equipment.service';
import {EmployeeService} from '../../structure-organizational/employee/services/employee.service';


@Component({
  selector: 'app-control-daily-equipments',
  templateUrl: './control-daily-equipments.component.html',
  styleUrls: ['./control-daily-equipments.component.scss']
})
export class ControlDailyEquipmentsComponent implements OnInit {

  employees: any;
  employeeId: number;
  employeesOptions: any;
  equipmentsAssigned: any;
  metadata: any;
  actions: any;
  equipmentsSelected: any;
  equipmentIdSelected: number;
  constructor(
    private router: ActivatedRoute,
    private route: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private equipmentService: EquipmentService,
    private employeeService: EmployeeService,
    private controlDailyEquipmentsService: ControlDailyEquipmentsService)
  {
    this.metadata = EquipmentAssignedListConfig;
    this.actions = ControlDailyEquipmentsActionsConfig;
    this.equipmentsSelected = [];
  }

  ngOnInit() {
    this.equipmentsAssigned = [];

    this.employeeService.getAll().subscribe((employeeResult) => {
      this.employees = employeeResult;
      this.buildOptionEmployee(employeeResult);
    });
    this.addActions();
 }

 buildOptionEmployee(employeeResult) {
   const options = [];
    employeeResult.forEach((objectEmployee) => {
     options.push({key: objectEmployee.id, viewValue: objectEmployee.firstName + " " +objectEmployee.lastName});
   });
    this.employeesOptions = options;
 }

  selectEmployee(employeeId) {
    this.employeeId = employeeId;
    this.employeeService.getEquipmentsAssigned(this.employeeId).subscribe((equipmentsResult) => {
      if(equipmentsResult.length === 0) {
        this.equipmentsAssigned = [];
      } else {
        this.equipmentsAssigned = equipmentsResult;
      }

    });

  }

 addActions() {
    this.actions[0].callBack = this.selectCheckboxEquipment;
 }

 selectCheckboxEquipment = (idObject, $event) => {
  this.equipmentIdSelected = idObject;
  if($event.checked) {
    this.equipmentsSelected.push(this.equipmentIdSelected);
  } else {
    _.remove(this.equipmentsSelected, (n) => { return n === this.equipmentIdSelected;})
  }
   this.equipmentsSelected = _.uniq(this.equipmentsSelected);
 }

 save() {
    const isCompleteEquipments : boolean = this.equipmentsSelected.length === this.equipmentsAssigned.length;
    let listSelectedEquipments = isCompleteEquipments? [] : this.equipmentsSelected;
   this.controlDailyEquipmentsService.createControlDailyEquipment(this.employeeId, isCompleteEquipments, listSelectedEquipments).subscribe((result) => {
     this.successCreated(result);
   });
 }

  successCreated(entity: any) {
    this.toastr.success('El Registro fue exitoso.');
  }
}
