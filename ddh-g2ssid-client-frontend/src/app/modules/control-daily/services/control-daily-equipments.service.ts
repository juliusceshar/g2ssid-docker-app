import {Injectable} from '@angular/core';
import {Restangular} from 'ngx-restangular';

@Injectable()
export class ControlDailyEquipmentsService {
  private baseControl: any;

  constructor(private restangular: Restangular) {
    //this.baseControl = restangular.all('projects');
  }

  getAllWithSearch(params) {
    return this.baseControl.getList({search: params});
  }

  createControlDailyEquipment(employeeId: number, useComplete: boolean, lisEquipmentIds: [number]) {
    let listEquipments = useComplete ? [] : lisEquipmentIds;
    let data = {
      completeEquipments: useComplete,
      dateDay: "2018-06-03T13:10:34.614Z",
      employeeId: employeeId,
      equipmentsIncompleteIdList: listEquipments,
    };
    return this.restangular.one('employees', employeeId).customPOST(data, 'equipments/controlDaily', undefined, undefined);
  }


}
