import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material';
import { ControlDailyEquipmentsComponent} from './control-daily-equipments/control-daily-equipments.component';
import { ControlDailyEquipmentsService} from './services/control-daily-equipments.service';
import { MatListModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule,
    MatSelectModule,
    MatListModule
  ],
  declarations: [
    ControlDailyEquipmentsComponent
  ],
  providers:[
    ControlDailyEquipmentsService
  ],
  exports:[]
})
export class ControlDailyModule { }
