import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureOrganizationalComponent } from './structure-organizational.component';

describe('StructureOrganizationalComponent', () => {
  let component: StructureOrganizationalComponent;
  let fixture: ComponentFixture<StructureOrganizationalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StructureOrganizationalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StructureOrganizationalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
