import { Component, OnInit } from '@angular/core';
import {ContractConfig} from './contract.config';
import {PositionService} from '../position/services/position.service';
import { Contract } from '../../../shared/contract';
import { ContractService } from './services/contract.service';
import { EmployeeService } from '../employee/services/employee.service';
import { BaseComponent } from '../../../shared/baseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { NgIf } from '@angular/common';
import { EntityUtils } from '../../../utils/entityUtils';
import {ProjectService} from '../project/services/project.service';


@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent extends BaseComponent implements OnInit {
  employees: any;
  positions: any;
  projects: any;
  id;
  //config;

  constructor(
    private contractService: ContractService,
    private employeeService: EmployeeService,
    private positionService: PositionService,
    private  projectService: ProjectService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
      super(contractService, ContractConfig, router, route, toastr, spinner);
    }
  ngOnInit( ) {
    this.employeeService.getAll().subscribe(employees => {this.employees = employees; this.buildEmployeeDropdown(); });
    this.positionService.getAll().subscribe(positions => { this.positions = positions; this.buildPositionDropdown(); });
    this.projectService.getProjects().subscribe(projects => { this.projects = projects; this.buildProjectDropdown(); })
    super.ngOnInit();

  }
  loadEditMode = function () {
    this.spinner.show();
    this.baseService.get(this.id).subscribe(
      res => {
        res.employeeId = { key: res.employeeId, value: res.employee};
        res.positionId = { key: res.positionId, value: res.position};
        res.projectId =  { key: res.projectId, value: res.project};
        console.log( res.employeeId);
        EntityUtils.buildFormUpdate(this.metadata, res);
        this.ready = true;
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };
  successCreated(contract: any) {
    this.toastr.success('Fue creado exitosamente.', 'El Contrato: ' + contract.name);
  }
  successEdited(contract: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'El Contrato : ' + contract.name);
  }
  successDeleted(contract: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'El Contrato: ' + contract.name);
  }
  buildPositionDropdown = function() {
    const options = [];
    this.positions.forEach(function(position) {
     options.push({key: position.id , value: position.name});
    });
    this.metadata[this.metadata.length - 1].options = options;
  };
  buildEmployeeDropdown = function() {

    const options = [];
    this.employees.forEach(function(employee) {
        options.push({ key: employee.id, value: employee.firstName + ' ' + employee.lastName});
    });
    this.metadata[4].options = options;
  };

  buildProjectDropdown = function () {
    const optionsProject = [];
    this.projects.forEach(function (project) {
      optionsProject.push({ key: project.id, value: project.name});
    });
    console.log('tetxs');
    this.metadata[5].options = optionsProject;
  };

}
