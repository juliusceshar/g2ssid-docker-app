import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {map} from 'rxjs/operators';
import {Employee} from '../../../../shared/employee';
import {Contract} from '../../../../shared/contract';
import { baseURL } from '../../../../shared/baseurl';
import { BaseService } from '../../../../services/base.service';

@Injectable()
export class ContractService extends BaseService {
  constructor( http: HttpClient) {
    super(http, 'contracts/');
  }
/*@Injectable()
export class ContractService {
  constructor(private http: HttpClient) {
  }
  getContracts(): Observable<any> {
    return this.http.get(baseURL + 'contracts')
    .map((res) => {
        return res;
      }).catch(error => {
        console.log('error: ' + error);
        return error;
      });
  }
  getContract(id: number): Observable<Contract> {
    return this.http.get<Contract>(baseURL + 'contract/' + id)
      .map((res) => {
        return res;
      });
  }
  createContract(contract): Observable<any> {
    return this.http.post(baseURL + 'contracts', contract)
      .map((res) => {
        return res;
      });
  }*/

}
