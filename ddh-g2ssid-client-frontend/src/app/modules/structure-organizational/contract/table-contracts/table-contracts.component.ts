import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContractService } from '../services/contract.service';
import { ContractActionsConfig } from '../contract.actions.config';
import { ContractConfig } from './table-contract.config';


@Component({
  selector: 'app-table-contracts',
  templateUrl: './table-contracts.component.html',
  styleUrls: ['./table-contracts.component.scss']
})
export class TableContractsComponent implements OnInit {
  contracts;
  metadata;
  metadataActions;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private contractService: ContractService) {
    this.metadata = ContractConfig;
    this.metadataActions = ContractActionsConfig;
  }

  ngOnInit() {
    this.loadActions();
    this.loadContracts();
  }
  loadContracts() {
    this.contractService.getAll().subscribe(contract => this.contracts = contract);
  }

  loadActions() {
    this.metadataActions[0].callBack = this.goToEdit;
    ////this.metadataActions[1].callBack = this.delete;
  }
  delete = (id) => {
    this.contractService.delete(id).subscribe(res => this.loadContracts());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
}
