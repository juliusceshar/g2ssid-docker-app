
export const ContractConfig = [{
  'key': 'initDate',
  'label': 'Fecha Inicio Contrato',
  'type': 'datepicker',
  'required': true,
  'config': 'text',
  'value': ''
},
  {
    'key': 'endDate',
    'label': 'Fecha Final Contrato',
    'type': 'datepicker',
    'required': true,
    'config': 'text',
    'value': ''
  },
  {
    'key': 'salary',
    'label': 'Salario',
    'type': 'textbox',
    'required': true,
    'config': 'number',
    'value': ''
  },
  {
    'key': 'employeeType',
    'label': 'Tipo de Empleado',
    'type': 'dropdown',
    'required': true,
    'config': 'text',
    'value': '',
    'options': [
      {value: 'Tiempo completo', key: 'FULLTIME'},
      {value: 'Medio tiempo', key: 'HALFTIME'},
      {value: 'Tiempo parcial', key: 'PARTTIME'}
    ]
  },
    {
      'key': 'employee',
      'label': 'Empleado',
    },
    {
      'key': 'position',
      'label': 'Cargo',
    }];
