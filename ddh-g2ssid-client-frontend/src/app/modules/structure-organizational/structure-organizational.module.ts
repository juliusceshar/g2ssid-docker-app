import { ContractService } from './contract/services/contract.service';
import { PositionService } from './position/services/position.service';
import { ProjectService } from './project/services/project.service';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import { MatListModule} from '@angular/material';
import { RouterModule } from '@angular/router';
import { StructureOrganizationalComponent } from './main/structure-organizational.component';
import {EmployeeFormComponent} from './employee/employee-form/employee-form.component';
import {PositionComponent} from './position/position/position.component';
import {PositionListComponent} from './position/position-list/position-list.component';
import { ProjectComponent } from './project/project/project.component';
import { ContractComponent } from './contract/contract.component';

//Components
import { TableProjectsComponent} from './project/table-projects/table-projects.component';
import { DepartmentComponent} from './department/department/department.component';
import {TableEmployeesComponent} from './employee/table-employees/table-employees.component';
import { TableDepartmentsComponent} from './department/table-departments/table-departments.component';
import { TableContractsComponent } from './contract/table-contracts/table-contracts.component';

//Services
import {DepartmentService} from './department/services/department.service';
import {EmployeeService} from './employee/services/employee.service';
import { AssignDepartmentsComponent } from './project/assign-departments/assign-departments.component';
import { AssignPositionsComponent } from './department/assign-positions/assign-positions.component';
import { AssignEquipmentComponent } from './employee/assign-equipment/assign-equipment.component';
import { ReturnEquipmentComponent } from './employee/return-equipment/return-equipment.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatToolbarModule,
    RouterModule,
    MatSelectModule,
    MatListModule,

  ],
  declarations: [
    EmployeeFormComponent,
    TableEmployeesComponent,
    PositionComponent,
    PositionListComponent,
    ProjectComponent,
    ContractComponent,
    StructureOrganizationalComponent,
    DepartmentComponent,
    TableDepartmentsComponent,
    TableProjectsComponent,
    AssignDepartmentsComponent,
    AssignPositionsComponent,
    AssignEquipmentComponent,
    TableContractsComponent,
    ReturnEquipmentComponent
  ],
  exports: [
    EmployeeFormComponent,
    TableEmployeesComponent,
    PositionComponent,
    PositionListComponent,
    ContractComponent,
    StructureOrganizationalComponent,
    DepartmentComponent
  ],
  providers: [
    EmployeeService,
    PositionService,
    ProjectService,
    ContractService,
    DepartmentService
  ],
  entryComponents: [
    AssignEquipmentComponent,
    AssignDepartmentsComponent,
    AssignPositionsComponent,
    ReturnEquipmentComponent
  ],
})
export class OrganizationalStructureModule { }
