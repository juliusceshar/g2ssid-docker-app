import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MAT_SELECTION_LIST_VALUE_ACCESSOR} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { EquipmentService } from './../../../equipment-inventory/equipment/services/equipment.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-return-equipment',
  templateUrl: './return-equipment.component.html',
  styleUrls: ['./return-equipment.component.scss']
})
export class ReturnEquipmentComponent implements OnInit {

  employeeId:number;
  equipmentId:number;
  employee;
  listEquipments;

  constructor(
    private router: ActivatedRoute,
    private toastr: ToastrService,
    private equipmentService: EquipmentService,
    private employeeService: EmployeeService,
    public dialogRef: MatDialogRef<ReturnEquipmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

     this.employeeId = data.employeeId;
 }

 ngOnInit() {

  this.employeeService.get(this.employeeId).subscribe((employee) => { console.log('employee', employee), this.employee = employee;});
  this.employeeService.getEquipmentsAssigned(this.employeeId).subscribe((equipments) => {
    this.listEquipments = this.buildOptionsEquipments(equipments);
  });
  this.equipmentService.getAll().subscribe((equipments) => {this.listEquipments = equipments;});
}

buildOptionsEquipments = function(listEquipments) {
  const options = [];
  listEquipments.forEach(function(equipment) {
      options.push({ value: equipment.id, viewValue: equipment.code});
  });
  return options;
}


selectEquipmentId(equipmentId) {
  this.equipmentId = equipmentId;
 }

//  emitSaveEvent() {
//    this.employeeService.returnEquipmentByEmployee(this.employeeId, this.equipmentId).subscribe((saveObject) => {console.log('guardado', saveObject)});
//    this.toastr.success('Acción realizada con exito');
//  }

 onSubmit() {
  this.dialogRef.close();
}

}
