export const EmployeeConfig = [{
    'key': 'firstName',
    'label': 'Nombre(s)',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'pattern': '[a-zA-Z ]*',
    'error': 'Solo letras son permitidas.',
    'value': ''
  },
{
    'key': 'lastName',
    'label': 'Apellidos',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'ci',
    'label': 'C.I.',
    'type': 'textbox',
    'required': true,
    'config': 'number',
    'value': ''
},
{
    'key': 'birthDate',
    'label': 'Fecha de Nacimiento: dd/mm/aa',
    'type': 'datepicker',
    'required': true,
    'config': 'text',
    'value': '1/1/1990'
},
{
    'key': 'gender',
    'label': 'Sexo:Masculino/Femenino',
    'type': 'RadioButton',
    'value': {value: '', key: ''},
    'required': true,
    'options': [
        {value: 'Femenino', key: 'FEMALE'},
        {value: 'Masculino', key: 'MALE'}
    ]
},
{
    'key': 'address',
    'label': 'Direccion',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
},
{
    'key': 'phoneNumber',
    'label': 'Numero Telefonico',
    'type': 'textbox',
    'required': true,
    'config': 'number',
    'value': ''
},
{
    'key': 'email',
    'label': 'Email',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
}

];
