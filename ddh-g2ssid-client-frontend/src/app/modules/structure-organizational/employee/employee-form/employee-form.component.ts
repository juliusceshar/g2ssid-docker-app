import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../services/employee.service';
import {EmployeeConfig} from './employee.config';
import {Employee} from '../../../../shared/employee';
import { BaseComponent } from '../../../../shared/baseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent extends BaseComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(employeeService, EmployeeConfig, router, route, toastr, spinner);
  }

  successCreated(employee: any) {
    this.toastr.success('Fue creado exitosamente.', 'El Empleado: ' + employee.name);
  }

  successEdited(employee: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'El Empleado: ' + employee.name);
  }

  successDeleted(employee: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'El Empleado: ' + employee.name);
  }
}


