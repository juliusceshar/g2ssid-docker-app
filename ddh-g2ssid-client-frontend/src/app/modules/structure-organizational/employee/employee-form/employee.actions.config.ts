export const EmployeesActionsConfig = [
    {
      'label': 'Editar',
      'callBack': '',
      'hasIcon': 'fa fa-pencil-square-o fa-lg'
    },
    {
      'label': 'Eliminar',
      'callBack': '',
      'hasIcon': 'fa fa-trash fa-lg'
    },
    {
      'label': 'Asignar Equipo',
      'callBack': '',
    },
    {
      'label': 'Devolución Equipo',
      'callBack': '',
    },
  ];
