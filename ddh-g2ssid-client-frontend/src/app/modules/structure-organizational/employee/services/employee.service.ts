import { Injectable } from '@angular/core';
import {Employee} from '../../../../shared/employee';
import {baseURL} from '../../../../shared/baseurl';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Headers} from '@angular/http';
import {BaseService} from '../../../../services/base.service';
import {Restangular} from 'ngx-restangular';


@Injectable()
export class EmployeeService extends BaseService {
  lisCat;

  constructor(http: HttpClient) {
    super(http, 'employees/');
  }

  assignEquipmentToEmployee(employeeId, equimentId): Observable<any> {
    let data = { active: true, employeeId: employeeId, endDate: "2018-06-01T21:39:21.069Z",
      equipmentId: equimentId, initDate: "2018-06-01T21:39:21.069Z"};
    return this.http.post(baseURL +"equipment-assignment/", data)
      .map((res) => {
        console.log(res);
        return res;
      })
  }

  getEquipmentsAssigned(employeeId): Observable<any> {
    console.log('send');
    return this.http.get(this.endPoint + employeeId +"/equipment-assignment/").map((res) => {
      console.log('getEquipments', res);
      return res;
    });
  }

  // returnEquipmentByEmployee(equipmnetId): Observable<any> {
  //   return this.http.put(this.endPoint + equipmentId+ "")
  //     .map((res) => {
  //       return res;
  //     });
  // }

}
