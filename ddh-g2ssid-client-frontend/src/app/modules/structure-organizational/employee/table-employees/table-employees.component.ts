import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { EmployeeService } from '../services/employee.service';
import {EmployeeConfig} from '../employee-form/employee.config';
import {Employee} from '../../../../shared/employee';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesActionsConfig } from '../employee-form/employee.actions.config';
import { AssignEquipmentComponent } from '../assign-equipment/assign-equipment.component';
import { ReturnEquipmentComponent } from '../return-equipment/return-equipment.component';

@Component({
  selector: 'app-table-employees',
  templateUrl: './table-employees.component.html',
  styleUrls: ['./table-employees.component.scss']
})
export class TableEmployeesComponent implements OnInit {
  metadata;
  employees: [any];
  actions;

  constructor(private employeeService: EmployeeService, 
    public dialogRef: MatDialog,
    private router: Router,
    private route: ActivatedRoute) {
    this.metadata = EmployeeConfig;
    this.actions = EmployeesActionsConfig;
  }

  ngOnInit() {
    this.loadEmployees();
    this.loadActions();

  }
  loadEmployees() {
    this.employeeService.getAll().subscribe(result => this.employees = result );
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
    this.actions[2].callBack = this.openDialogAssignEquipment;
    this.actions[3].callBack = this.openDialogReturnEquipment;
  }

  delete = (id) => {
    this.employeeService.delete(id).subscribe(res => this.loadEmployees());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
  openDialogAssignEquipment = (index) => {
    let employeesSelected = this.employees[--index];
  this.dialogRef.open(AssignEquipmentComponent, {width: '500px' , height: '550px', data: {employeeId:employeesSelected.id}});
  }
  openDialogReturnEquipment = (index) => {
    let employeesSelected = this.employees[--index];
   this.dialogRef.open(ReturnEquipmentComponent, {width: '500px' , height: '500px', data: {employeeId:employeesSelected.id}});
  }
}
