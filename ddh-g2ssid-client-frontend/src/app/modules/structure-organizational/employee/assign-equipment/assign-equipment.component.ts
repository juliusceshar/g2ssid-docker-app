import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MAT_SELECTION_LIST_VALUE_ACCESSOR} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { EquipmentService } from './../../../equipment-inventory/equipment/services/equipment.service';
import { EquipmentCategoryService } from '../../../equipment-inventory/equipment-category/services/equipment-category.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { EquipmentListConfig } from './equipment-list.config';
import { AssignEquipmentsActionsConfig } from './assign-equipment.actions.config';
import { ReturnEquipmentComponent } from '../return-equipment/return-equipment.component';


@Component({
  selector: 'app-assign-equipment',
  templateUrl: './assign-equipment.component.html',
  styleUrls: ['./assign-equipment.component.scss']
})
export class AssignEquipmentComponent implements OnInit {

  employeeId:number;
  equipmentId:number;
  metadata:any;
  equipmentsAssigned: any;
  actions: any;
  employee;
  categoriesAll;
  listCategories;
  listEquipments;
  listTypes = [
    {value: 'SAFETY', viewValue: 'Equipo de Seguridad'},
    {value: 'OPERATIONAL', viewValue: 'Equipo Operacional'}
  ];

   constructor(
     private router: ActivatedRoute,
     private toastr: ToastrService,
     private equipmentService: EquipmentService,
     private employeeService: EmployeeService,
     private categoryService: EquipmentCategoryService,
     public dialogRef: MatDialogRef<AssignEquipmentComponent>,
     public dialogRefReturn: MatDialogRef<ReturnEquipmentComponent>,

     @Inject(MAT_DIALOG_DATA) public data: any) {

      this.employeeId = data.employeeId;
      this.equipmentId = data.equipmentId;
      this.metadata = EquipmentListConfig;
      this.actions = AssignEquipmentsActionsConfig;
      
  }

  ngOnInit() {

    this.employeeService.get(this.employeeId).subscribe((employee) => { console.log('employee', employee), this.employee = employee;});
    this.equipmentService.getAll().subscribe((equipments) => {this.listEquipments = equipments;});
    this.listEquipmentsAssigned();
  }

  buildOptionsCategories = function(listCategoriesEquipments) {
    const options = [];
    listCategoriesEquipments.forEach(function(category) {
        options.push({ value: category.id, viewValue: category.name});
    });
    return options;
  }

  buildOptionsEquipments = function(listEquipments) {
    const options = [];
    listEquipments.forEach(function(equipment) {
        options.push({ value: equipment.id, viewValue: equipment.code});
    });
    return options;
  }

  selectTypeCategory(typeCategory) {
    this.listCategories = [];
    this.categoryService.getCategoriesEquipmentsByType(typeCategory).subscribe((categoriesEquipments) => {
    this.listCategories = this.buildOptionsCategories(categoriesEquipments);
   });
 }

  selectCategory(categoryId) {
    this.listEquipments=[];
    this.categoryService.getEquipmentsByCategory(categoryId).subscribe((equipments) => {
    this.listEquipments = this.buildOptionsEquipments(equipments);
    });
  }

  selectEquipment(equipmentId) {
    this.equipmentId = equipmentId;
   }

   emitSaveEvent() {
     this.employeeService.assignEquipmentToEmployee(this.employeeId, this.equipmentId).subscribe((saveObject) => {console.log('guardado', saveObject)});
     this.toastr.success('Fue asignado correctamente el equipo');
   }

   listEquipmentsAssigned() {
    this.employeeService.getEquipmentsAssigned(this.employeeId).subscribe((equipmentsResult) => {
      if(equipmentsResult.length === 0) {
        this.equipmentsAssigned = [];
      } else {
        this.equipmentsAssigned = equipmentsResult;
        console.log(this.equipmentsAssigned) ;
      }
    });
  }

   onSubmit() {
    this.dialogRef.close();
  }

}
