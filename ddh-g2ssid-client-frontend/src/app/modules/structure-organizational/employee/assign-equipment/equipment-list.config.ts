export const EquipmentListConfig = [
    {
      'key': 'code',
      'label': 'Codigo de Equipo',
      'type': 'textbox'
    },
    {
      'key': 'state',
      'label': 'Estado',
      'type': 'textbox'
    },
    {
      'key': 'type',
      'label': 'Categoria de Equipo',
      'type': 'dropdown'
    }
  ];