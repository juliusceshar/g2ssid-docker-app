export const DepartmentConfig = [{
  'key': 'name',
  'label': 'Nombre del Departamento',
  'type': 'textbox',
  'required': true,
  'config': 'text',
  'value': ''
},
  {
    'key': 'description',
    'label': 'Descripcion del Departamento',
    'type': 'textarea',
    'required': true,
    'config': 'textarea',
    'value': ''
  }
];
