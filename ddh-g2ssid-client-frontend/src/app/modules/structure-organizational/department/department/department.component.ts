import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/baseComponent';
import {FormControl, Validators, NgForm} from '@angular/forms';
import {Department} from '../../../../shared/department';
import {DepartmentService} from '../services/department.service';
import {DepartmentConfig} from './department.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent extends BaseComponent implements OnInit {

  constructor(
    private departmentService: DepartmentService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(departmentService, DepartmentConfig, router, route, toastr, spinner);
  }

  successCreated(department: any) {
    this.toastr.success('Fue creado exitosamente.', 'El Departamento: ' + department.name);
  }

  successEdited(department: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'El Departamento: ' + department.name);
  }

  successDeleted(department: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'El Departamento: ' + department.name);
  }
}


