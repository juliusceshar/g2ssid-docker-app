import {Injectable} from '@angular/core';
import { BaseService } from '../../../../services/base.service';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Restangular} from 'ngx-restangular';

@Injectable()
export class DepartmentService extends BaseService {

  constructor(http: HttpClient, private restangular: Restangular) {
    super(http, 'departments/');
  }


  createAssignPositions(departmentId: number, lisPositionsIds: any) {
    if (lisPositionsIds != null) {
      return this.restangular.one('departments', departmentId).customPOST({'listPositions': lisPositionsIds}, 'positions/assignPositions', undefined, undefined );
    }
  }

  getPositions(deparmentId: number) {
    return this.restangular.one('departments', deparmentId).getList('positions');
  }
}
