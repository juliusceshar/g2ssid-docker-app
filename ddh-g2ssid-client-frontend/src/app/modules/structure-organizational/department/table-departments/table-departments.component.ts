import { DepartmentsActionsConfig } from './../department/department.actions.config';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog} from '@angular/material';
import {DepartmentConfig} from '../department/department.config';
import {DepartmentService} from '../services/department.service';
import {AssignPositionsComponent} from '../assign-positions/assign-positions.component';



@Component({
  selector: 'app-table-departments',
  templateUrl: './table-departments.component.html',
  styleUrls: ['./table-departments.component.scss']
})
export class TableDepartmentsComponent implements OnInit {
  metadata;
  departments: [any];
  actions;

  constructor(private departmentService: DepartmentService, public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute) {
    this.metadata = DepartmentConfig;
    this.actions = DepartmentsActionsConfig;
  }

  ngOnInit() {
    this.loadDepartments();
    this.loadActions();
  }

  loadDepartments() {
    this.departmentService.getAll().subscribe(result => this.departments = result );
  }
  loadActions() {
    this.actions[0].callBack = this.openDialogAssignPositions;
    this.actions[1].callBack = this.goToEdit;
    this.actions[2].callBack = this.delete;
  }

  delete = (id) => {
    this.departmentService.delete(id).subscribe(res => this.loadDepartments());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }

  openDialogAssignPositions = (index) => {
    let departmentSelected = this.departments[--index];
    this.dialog.open(AssignPositionsComponent, {width: '500px' , height: '400px', data: { departmentId: departmentSelected.id}});
  }
}
