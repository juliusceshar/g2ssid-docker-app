import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDepartmentsComponent } from './table-departments.component';

describe('TableProjectsComponent', () => {
  let component: TableDepartmentsComponent;
  let fixture: ComponentFixture<TableDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
