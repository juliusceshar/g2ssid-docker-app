import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AssignPositionConfig} from './AssignPositionConfig';
import {AssignDepartmentsComponent} from '../../project/assign-departments/assign-departments.component';
import {ActivatedRoute} from '@angular/router';
import {DepartmentService} from '../services/department.service';
import {PositionService} from '../../position/services/position.service';

@Component({
  selector: 'app-assign-positions',
  templateUrl: './assign-positions.component.html',
  styleUrls: ['./assign-positions.component.scss']
})
export class AssignPositionsComponent implements OnInit {
  metadata: object;
  departmentId: number;
  department: any;
  listPositions: any;
  listSelectedPositions: any;
  selectedOptions: number[];
  options: {
    id: number;
    name: string;
    selected: boolean;
  }[] = [];
  constructor(private router: ActivatedRoute, private departmentService: DepartmentService, private positionService: PositionService, public dialogRef: MatDialogRef<AssignDepartmentsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.metadata = AssignPositionConfig;
    this.departmentId = data.departmentId;
  }

  ngOnInit() {
    this.departmentService.get(this.departmentId).subscribe((department) => { this.department = department;});
    this.positionService.getAll().subscribe((positions) => {
      this.listPositions = positions;
      this.departmentService.getPositions(this.departmentId).subscribe((positionsSelected) => {
        this.listSelectedPositions = positionsSelected;
        this.buildOptionsCheckGroup();
      });
    });
  }

  buildOptionsCheckGroup = function() {
    this.options = [];

    this.listPositions.forEach((position) => {
      const isSelected: boolean = this.listSelectedPositions.find((element) => {
        return element.id === position.id});
      this.options.push({key: position.id, value: position.name, selected: isSelected});
    });
    this.metadata[0].options = this.options;
    this.metadata[0].callBack = this.onPositionsListControlChanged;
  };

  onPositionsListControlChanged = (list) => {
    // determine selected options
    this.selectedOptions = list.selectedOptions.selected.map(item => item.value);
  }

  save = function (positionObject) {
    this.departmentService.createAssignPositions(this.departmentId, this.selectedOptions).subscribe((data) => { this.dialogRef.close(); });
  };

  onSubmit() {
    this.dialogRef.close();
  }

}
