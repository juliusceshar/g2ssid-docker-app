import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignPositionsComponent } from './assign-positions.component';

describe('AssignPositionsComponent', () => {
  let component: AssignPositionsComponent;
  let fixture: ComponentFixture<AssignPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
