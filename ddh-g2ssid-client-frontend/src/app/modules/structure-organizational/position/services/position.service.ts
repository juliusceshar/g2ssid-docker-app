import { BaseService } from '../../../../services/base.service';
import {Injectable} from '@angular/core';
import {Position} from '../../../../shared/position';
import {baseURL} from '../../../../shared/baseurl';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Headers} from '@angular/http';

@Injectable()
export class PositionService extends BaseService {

  constructor(http: HttpClient) {
    super(http, 'positions/');
  }
}
