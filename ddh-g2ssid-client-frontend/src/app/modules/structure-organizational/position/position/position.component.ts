import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/baseComponent';
import {PositionService} from '../services/position.service';
import {PositionConfig} from './position.config';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent extends BaseComponent implements OnInit {

  constructor(
    private positionService: PositionService,
    router: ActivatedRoute,
    route: Router,
    spinner: NgxSpinnerService,
    toastr: ToastrService) {
    super(positionService, PositionConfig, router, route, toastr, spinner);
  }

  successCreated(position: any) {
    this.toastr.success('Fue creado exitosamente.', 'El Cargo: ' + position.name);
  }

  successEdited(position: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'El Cargo: ' + position.name);
  }

  successDeleted(position: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'El Cargo: ' + position.name);
  }
}
