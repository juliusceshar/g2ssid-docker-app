import { Component, OnInit } from '@angular/core';
import {PositionConfig} from '../position/position.config';
import {PositionService} from '../services/position.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PositionsActionsConfig } from '../position/position.actions.config';

@Component({
  selector: 'app-position-list',
  templateUrl: './position-list.component.html',
  styleUrls: ['./position-list.component.scss']
})
export class PositionListComponent implements OnInit {
  metadata;
  positions: [any];
  actions;

  constructor(private positionService: PositionService,
    private router: Router,
    private route: ActivatedRoute) {
    this.metadata = PositionConfig;
    this.actions = PositionsActionsConfig;
  }

  ngOnInit() {
    this.loadPositions();
    this.loadActions();
  }

  loadPositions() {
    this.positionService.getAll().subscribe(result => this.positions = result );
  }

  loadActions() {
    this.actions[0].callBack = this.goToEdit;
    this.actions[1].callBack = this.delete;
  }

  delete = (id) => {
    this.positionService.delete(id).subscribe(res => this.loadPositions());
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
}
