import {Injectable} from '@angular/core';

import {Restangular} from 'ngx-restangular';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class ProjectService {
  private baseProjects: any;
  private currentProject: any;
  constructor(private restangular: Restangular) {
    this.baseProjects = restangular.all('projects');
  }

  getProjects() {
    return this.baseProjects.getList();
  }

  getAllWithSearch(params) {
    return this.baseProjects.getList({search: params});
  }

  getProjectById(projectId) {
    return this.baseProjects.get(projectId);
  }

  create(project) {
    return this.baseProjects.post(project);
  }

  update(project) {return project.put();}

  get(id) {
    return this.baseProjects.get(id);
  }


  /**
   * Assign Departaments to a project.
   * @param {number} projectId
   * @param lisDepartmentsIds
   */
  createAssignDepartments(projectId: number, lisDepartmentsIds: any) {
    if (lisDepartmentsIds != null) {
      return this.restangular.one('projects', projectId).customPOST({'listDepartments': lisDepartmentsIds}, 'departments/assignDepartments', undefined, undefined );
    }
  }

  getDepartments(projectId: number) {
    return this.restangular.one('projects', projectId).getList('departments');
  }
}
