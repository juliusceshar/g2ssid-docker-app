import {Component, Inject, OnInit} from '@angular/core';
import {AssignDepartmentConfig} from './assignDepartments.config';
import {ProjectService} from '../services/project.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DepartmentService} from '../../department/services/department.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-assign-departments',
  templateUrl: './assign-departments.component.html',
  styleUrls: ['./assign-departments.component.scss']
})
export class AssignDepartmentsComponent implements OnInit {
  metadata: object;
  projectId: number;
  project: any;
  listDepartments: any;
  listSelectedDepartments: any;
  selectedOptions: number[];
  options: {
    id: number;
    name: string;
    selected: boolean;
  }[] = [];
  constructor(private projectService: ProjectService, private router: ActivatedRoute, private departmentService: DepartmentService, public dialogRef: MatDialogRef<AssignDepartmentsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.metadata = AssignDepartmentConfig;
    this.projectId = data.projectId;
  }
  ngOnInit() {
    this.projectService.getProjectById(this.projectId).subscribe((project) => { this.project = project;});
    this.departmentService.getAll().subscribe((departments) => {
      this.listDepartments = departments;
      this.projectService.getDepartments(this.projectId).subscribe((departmentsSelected) => {
        this.listSelectedDepartments = departmentsSelected;
        this.buildOptionsCheckGroup();
      });
    });
  }

  buildOptionsCheckGroup = function() {
    this.options = [];

    this.listDepartments.forEach((department) => {
      const isSelected: boolean = this.listSelectedDepartments.find((element) => {
        return element.id === department.id});
      this.options.push({key: department.id, value: department.name, selected: isSelected});
    });
    this.metadata[0].options = this.options;
    this.metadata[0].callBack = this.onDepartmentsListControlChanged;
  };

  onDepartmentsListControlChanged = (list) => {
    // determine selected options
    this.selectedOptions = list.selectedOptions.selected.map(item => item.value);
  }

  save = function (departmentObject) {
    this.projectService.createAssignDepartments(this.projectId, this.selectedOptions).subscribe((data) => { this.dialogRef.close(); });
  };

  onSubmit() {
    this.dialogRef.close();
  }
}
