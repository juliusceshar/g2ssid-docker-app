export const ProjectConfig = [{
    'key': 'name',
    'label': 'Nombre del Proyecto',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
  },
  {
    'key': 'description',
    'label': 'Descripcion del Proyecto',
    'type': 'textarea',
    'required': true,
    'config': 'textarea',
    'value': ''
  },
  {
    'key': 'place',
    'label': 'Lugar del Proyecto',
    'type': 'textbox',
    'required': true,
    'config': 'text',
    'value': ''
  },
  {
    'key': 'statusProject',
    'label': 'Estado del Proyecto',
    'type': 'dropdown',
    'required': true,
    'config': 'text',
    'value': {key: '', value: ''},
    'options': [
      {value: 'Creado', key: 'CREATED', viewValue: 'Creado'},
      {value: 'Iniciado', key: 'STARTED', viewValue: 'Iniciado'},
      {value: 'Completado', key: 'COMPLETED', viewValue: 'Completado'},
      {value: 'Cancelado', key: 'CANCELLED', viewValue: 'Cancelado'},
      {value: 'Pausado', key: 'ONHOLD', viewValue: 'Pausado'}
    ]
  }
];

