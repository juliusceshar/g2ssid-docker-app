import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as _ from 'lodash';
import {BaseComponent} from '../../../../shared/baseComponent';
import {ProjectService} from '../services/project.service';
import {EntityUtils} from '../../../../utils/entityUtils';
import {ToastrService} from 'ngx-toastr';
import {ProjectConfig} from './project.config';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent extends BaseComponent implements OnInit {
  private currentProject: any;
  departments: any;
  ready: boolean;
  
  constructor( private projectService: ProjectService,
               router: ActivatedRoute,
               route: Router,
               spinner: NgxSpinnerService,
               toastr: ToastrService) {
    super(projectService, ProjectConfig, router, route, toastr, spinner);    
  }
  
  loadEditMode = function () {
    this.baseService.get(this.id).subscribe(
      (res) => {
        EntityUtils.buildFormUpdate(this.metadata, res);
        this.currentProject = res;
        this.ready = true;
      }
    );
  };

  update = function (value) {
    value.id = this.id;
    this.spinner.show();
    _.merge(this.currentProject, value);
    this.baseService.update(this.currentProject).subscribe(
      res => {
        this.router.navigate(['../'], { relativeTo: this.route });
        this.successEdited(res);
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };

  successCreated(project: any) {
    this.toastr.success('Fue creado exitosamente.', 'El Proyecto: ' + project.name);
  }

  successEdited(project: any) {
    this.toastr.success('Fue actualizado exitosamente.', 'El Proyecto: ' + project.name);
  }

  successDeleted(project: any) {
    this.toastr.success('Fue eliminado exitosamente.', 'El Proyecto: ' + project.name);
  }
}
