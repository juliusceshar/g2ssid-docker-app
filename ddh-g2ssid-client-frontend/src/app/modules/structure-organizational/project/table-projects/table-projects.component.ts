import { Component, OnInit } from '@angular/core';

import {ProjectService} from '../services/project.service';
import {ProjectConfig} from '../project/project.config';
import {ProjectActionsConfig} from '../project/project.actions.config';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {AssignDepartmentsComponent} from '../assign-departments/assign-departments.component';

@Component({
  selector: 'app-table-projects',
  templateUrl: './table-projects.component.html',
  styleUrls: ['./table-projects.component.scss']
})
export class TableProjectsComponent implements OnInit {
  projects;
  metadata;
  metadataActions;
  constructor( private router: Router, private route: ActivatedRoute, private projectService: ProjectService, public dialog: MatDialog) {
    this.metadata = ProjectConfig;
    this.metadataActions = ProjectActionsConfig;
  }

  ngOnInit() {
    this.projectService.getProjects().subscribe(projects => this.projects = projects);
    this.metadataActions[0].callBack = this.openDialogAssignDepartments;
    this.metadataActions[1].callBack = this.goToEdit;
  }

  openDialogAssignDepartments = (idObject) => {
    this.dialog.open(AssignDepartmentsComponent, {width: '500px' , height: '400px', data: { projectId: idObject}});
  }

  goToEdit = (id) => {
    this.router.navigate(['./' + id], { relativeTo: this.route });
  }
}
