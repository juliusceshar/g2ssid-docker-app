import { Component } from '@angular/core';
import {Routes} from '@angular/router';
import {HomeComponent} from '../home/home.component';
//import { EquipmentComponent } from '../modules/equipment-inventory/equipment/equipment.component';
import { EquipmentAssignComponent } from '../modules/equipment-inventory/equipment-assign/equipment-assign.component';
import { EquipmentComponent } from '../modules/equipment-inventory/equipment/equipment/equipment.component';
import { EquipmentListComponent } from '../modules/equipment-inventory/equipment/equipment-list/equipment-list.component';
import { TableEmployeesComponent } from '../modules/structure-organizational/employee/table-employees/table-employees.component';
import {EmployeeFormComponent} from '../modules/structure-organizational/employee/employee-form/employee-form.component';
import {ProjectComponent} from '../modules/structure-organizational/project/project/project.component';
import {ContractComponent} from '../modules/structure-organizational/contract/contract.component';
import {StructureOrganizationalComponent} from '../modules/structure-organizational/main/structure-organizational.component';
import { PositionComponent } from '../modules/structure-organizational/position/position/position.component';
import { PositionListComponent } from '../modules/structure-organizational/position/position-list/position-list.component';
import { EquipmentInventoryComponent } from '../modules/equipment-inventory/main/equipment-inventory.component';
import { UserManagementComponent } from '../modules/user-management/main/user-management.component';
import {UserComponent} from '../modules/user-management/user/user/user.component';
import {UserListComponent} from '../modules/user-management/user/user-list/user-list.component';
import {DepartmentComponent} from '../modules/structure-organizational/department/department/department.component';
import {TableDepartmentsComponent} from '../modules/structure-organizational/department/table-departments/table-departments.component';
import {TableProjectsComponent} from '../modules/structure-organizational/project/table-projects/table-projects.component';
import {AssignDepartmentsComponent} from '../modules/structure-organizational/project/assign-departments/assign-departments.component';
import { EquipmentCategoryCardComponent } from '../modules/equipment-inventory/equipment-category/equipment-category-card/equipment-category-card.component';
import {EquipmentCategoryComponent} from '../modules/equipment-inventory/equipment-category/equipment-category/equipment-category.component';
import { ReportComponent } from '../modules/reports/main/report.component';
import {AssignPositionsComponent} from '../modules/structure-organizational/department/assign-positions/assign-positions.component';
import {AssignEquipmentComponent} from '../modules/structure-organizational/employee/assign-equipment/assign-equipment.component';
import { EmployeeReportComponent } from '../modules/reports/employee-report/employee-report.component';
import { EquipmentCategoryDetailComponent } from './../modules/equipment-inventory/equipment-category/equipment-category-detail/equipment-category-detail.component';
import { AccidentFormComponent } from '../modules/accidents/accident-form/accident-form.component';
import { AccidentListComponent } from '../modules/accidents/accident-list/accident-list.component';
import {ProjectReportComponent} from '../modules/reports/project-report/project-report.component';
import {EquipmentReportComponent} from '../modules/reports/equipment-report/equipment-report.component';
import { MainComponent } from '../modules/accidents/main/main.component';
import {ControlDailyEquipmentsComponent} from '../modules/control-daily/control-daily-equipments/control-daily-equipments.component';
import { TableContractsComponent } from '../modules/structure-organizational/contract/table-contracts/table-contracts.component';
import { ContractReportComponent } from '../modules/reports/contract-report/contract-report.component';
import { AccidentReportComponent } from '../modules/reports/accident-report/accident-report.component';
import { DepartmentReportComponent } from './../modules/reports/department-report/department-report.component';
import { EquipmentCategoryReportComponent } from '../modules/reports/equipment-category-report/equipment-category-report.component';

export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'user-management',
    component: UserManagementComponent,
    children: [
      {path: 'users/create', component: UserComponent},
      {path: 'users', component: UserListComponent},
      {path: 'users/:id', component: UserComponent}
     ]
  },
  {
    path: 'reports',
    component: ReportComponent,
    children: [
      {path: 'employees', component: EmployeeReportComponent},
      {path: 'projects', component: ProjectReportComponent},
      {path: 'equipments', component: EquipmentReportComponent},
      {path: 'equipments-category', component: EquipmentCategoryReportComponent },
      {path: 'contracts', component: ContractReportComponent},
      {path: 'accidents', component: AccidentReportComponent},
      {path: 'departments', component: DepartmentReportComponent}
    ]
  },
  {
    path: 'equipment-inventory',
    component: EquipmentInventoryComponent,
    children: [
      {path: 'equipments', component: EquipmentListComponent},
      {path: 'equipments/:id', component: EquipmentComponent},
      {path: 'equipments/create', component: EquipmentComponent},
      {path: 'equipment-categories', component: EquipmentCategoryCardComponent},
      {path: 'equipment-categories/safety', component: EquipmentCategoryCardComponent},
      {path: 'equipment-categories/operational', component: EquipmentCategoryCardComponent},
      {path: 'equipment-categories/:id', component: EquipmentCategoryComponent},
      {path: 'equipment-categories/create', component: EquipmentCategoryComponent},
      {path: 'equipment-categories/detail/:id', component: EquipmentCategoryDetailComponent},
      {path: 'equipment/assign', component: EquipmentAssignComponent},
      {path: 'equipment/controlDaily', component: ControlDailyEquipmentsComponent},

    ]
  },
  {
    path: 'structure-organizational',
    component: StructureOrganizationalComponent,
    children: [
      {path: 'employees/create', component: EmployeeFormComponent},
      {path: 'employees', component: TableEmployeesComponent},
      {path: 'employees/:id', component: EmployeeFormComponent},
      {path: 'positions/create', component: PositionComponent},
      {path: 'positions', component: PositionListComponent},
      {path: 'positions/:id', component: PositionComponent},
      {path: 'positions/:id/assignPositions', component: AssignPositionsComponent},
      {path: 'projects/create', component: ProjectComponent},
      {path: 'projects/:id/assignDepartments', component: AssignDepartmentsComponent},
      {path: 'projects', component: TableProjectsComponent},
      {path: 'projects/:id', component: ProjectComponent},
      {path: 'departments/create', component: DepartmentComponent},
      {path: 'departments', component: TableDepartmentsComponent},
      {path: 'departments/:id', component: DepartmentComponent},
      {path: 'contracts/create', component: ContractComponent},
      {path: 'contracts/:id' , component: ContractComponent},
      {path: 'contracts', component: TableContractsComponent}
    ]
  },
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'accidents',
    component: MainComponent,
    children: [
      {path: '', component: AccidentListComponent},
      {path: 'create', component: AccidentFormComponent},
      {path: ':id', component: AccidentFormComponent}
    ]
  },
  {path: 'accident-form', component: AccidentFormComponent},
  {path: 'control-daily-form', component: ControlDailyEquipmentsComponent}
];
