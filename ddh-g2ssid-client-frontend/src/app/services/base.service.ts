import {baseURL} from '../shared/baseurl';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Headers} from '@angular/http';

export class BaseService {

  endPoint: string;
  constructor(protected http: HttpClient, nameEntity: string) {
    this.endPoint = baseURL + nameEntity;
  }

  getAll(): Observable<any> {
    return this.http.get(this.endPoint)
    .map((res) => {
      return res;
    }).catch(error => {
      console.log('error: ' + error);
      return error;
    });
  }
  
  getAllWithSearch(params): Observable<any> {
    return this.http.get(this.endPoint+params)
    .map((res) => {
      return res;
    }).catch(error => {
      console.log('error: ' + error);
      return error;
    });
  }

  get(id: number): Observable<any> {
    return this.http.get<any>(this.endPoint + id)
      .map(res => {
        return res;
      });
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(this.endPoint + id)
      .map(res => {
        return res;
      });
  }

  create(entity): Observable<any> {
    return this.http.post(this.endPoint, entity)
      .map((res) => {
        return res;
      });
  }

  update(entity): Observable<any> {
    return this.http.put(this.endPoint + entity.id, entity)
      .map((res) => {
        return res;
      });
  }
}
