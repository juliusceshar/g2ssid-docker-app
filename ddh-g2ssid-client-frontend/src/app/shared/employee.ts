export interface Employee {
    address: string;
    birthDate: string;
    ci: number;
    email: string;
    firstName: string;
    gender: string;
    id: number;
    lastName: string;
    phoneNumber: number;
    picture: string;
}
