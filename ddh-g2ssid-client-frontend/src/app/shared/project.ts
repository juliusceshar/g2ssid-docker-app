export interface Project {
  id: number;
  name: string;
  description: string;
  place: string;
  statusProject: string;
}
