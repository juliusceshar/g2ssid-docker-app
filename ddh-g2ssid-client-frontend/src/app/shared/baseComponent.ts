import { Component, OnInit } from '@angular/core';
import { BaseService } from './../services/base.service';
import { EntityUtils } from './../utils/entityUtils';
import {FormControl, Validators, NgForm} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
export class BaseComponent implements OnInit {
  metadata;
  id;
  save;
  mode: String;
  ready: boolean;
  constructor(
    private baseService: any,
    private config: any,
    private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService) {
    this.metadata = JSON.parse(JSON.stringify(config));
    this.mode = 'Crear';
    this.ready = false;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      if (this.id) {
        this.mode = 'Editar';
        this.loadEditMode();
        this.save = this.update;
      } else {
        this.ready = true;
        this.save = this.create;
      }
   });
  }

  create = function (value) {
    this.spinner.show();
    this.baseService.create(value).subscribe(
      res => {
        this.router.navigate(['../'], { relativeTo: this.route });
        this.successCreated(res);
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };

  update = function (value) {
    value.id = this.id;
    this.spinner.show();
    this.baseService.update(value).subscribe(
      res => {
        this.router.navigate(['../'], { relativeTo: this.route });
        this.successEdited(res);
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };

  delete = function (value) {
    value.id = this.id;
    this.spinner.show();
    this.baseService.delete(value).subscribe(
      res => {
        this.router.navigate(['../'], { relativeTo: this.route });
        this.successDeleted(res);
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };

  loadEditMode = function () {
    this.spinner.show();
    this.baseService.get(this.id).subscribe(
      res => {
        EntityUtils.buildFormUpdate(this.metadata, res);
        this.afterEditMode();
        this.ready = true;
      },
      error => {
        this.errorMessage();
      },
      () => {
        this.spinner.hide();
      });
  };
  
  afterEditMode() {
    // method: to override if need a custom functionality.
  }

  successCreated(entity: any) {
    this.toastr.success('Fue creado exitosamente.');
  }

  successEdited(entity: any) {
    this.toastr.success('Fue actualizado exitosamente.');
  }

  successDeleted(entity: any) {
    this.toastr.success('Fue eliminado exitosamente.');
  }

  errorMessage() {
    this.toastr.error('Ocurrió un error.');
  }

}
