export interface Contract {
  id: number;
  salary: number;
  initDate: string;
  endDate: string;
  employeeId: number;
  positionId: number;
  employeeType: string;
}
