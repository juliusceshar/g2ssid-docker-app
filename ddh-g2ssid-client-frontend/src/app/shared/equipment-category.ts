export interface EquipmentCategory {
    title: string;
    id: number;
    name: string;
    image: string;

}
